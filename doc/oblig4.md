# Rapport – Innlevering 4

**Team:** Høytek Nightfall: Survivors Saga Deluxe Edition


## Teamroller
- **Magnus Fjeldstad**
  - **Rolle:** Team Lead

- **Lyder Samnøy**
  - **Rolle:** Game Designer

- **Brage Hogstad**
  - **Rolle:** Testansvarlig

- **Jens Brown Eriksen**
  - **Rolle:** Dokumentasjonsansvarlig

- **Johannes Helleve**
  - **Rolle:** Git Master

- **Henrik Tennebekk**
  - **Rolle:** Co-Team Lead


## Konsept
Vi har laget et Rouge Like 2D spill hvor du ser brettet ovenfra og har hentet inspirasjon fra vampire survivors Spillet består av en player, og 2 typer enemies.

Spilleren beveger seg på brettet, og har som mål skyte fiender med sitt "fireball"- våpen. Fiendene vil følge etter deg gjennom spillet. Når du har utslettet alle fiendene, fil det komme en ny "wave" med fiender. For hver wave, vil fiendene progressivt blir fler og fler.

Mellom rundene kan spilleren velge mellom forskjellige oppgraderinger som gir spilleren fordeler i spillet. Spilleren kan velge mellom oppgraderinger som gir mer helse, mer angrepskraft, eller raskere bevegelse. I tilleg kan spilleren plukke opp powerups som gir midlertidige fordeler, som høyere damage eller tempo.

Dersom spilleren mister alle livene sine, vil spillet være over og spilleren kan starte på nytt.

## Hvordan fungerer rollene i teamet?

Gjennom prosjektet har vi sett at rollene våre stort sett har fungert ganske bra. Vi har klart å fordele arbeidet godt og har hatt en grei oversikt over hvem som har gjort hva. Likevel, med forskjellige timeplaner og varierende tid tilgjengelig, har vi noen ganger måttet justere litt på rollene våre. Dette har betydd at vi har hjulpet hverandre med forskjellige oppgaver og at vi av og til har måttet ta på oss ting som ikke nødvendigvis ligger i vår vanlige rolle. Selv om det var litt utfordrende, har det faktisk vært en bra ting for oss alle. Det fikk oss til å lære mer om andre deler av prosjektet, noe som til syvende og sist gjorde teamet vårt sterkere.


## Hvilke erfaringer har vi fått i henhold til å være et team?


Etter endt prosjekt sitter vi stort sett igjen med gode opplevelser med det å være et team. Vi var i utgangspunktet en ganske tilfeldig satt sammen gruppe, hvor ikke alle kjente hverandre. Kun enkelte hadde relasjoner fra før. Likevel føler vi at vi rakst ble kjent, og fikk opp en god struktur for hvordan vi jobbet sammen. 

I starten av prosjektet holdt vi oss hovedsaklig til de ukentlige gruppetimene for å møtes i gruppen. Ellers jobbet vi induviduelt hvert vårt sted.  Dette fungerte greit, men da vi kom dyperer inn i prosjektet så vi de nødvendig å møtes jevnerer og i lengre perioder. Vi begynte da å jobbe oftere sammen på grupperom, slik at alle var tilstede og kunne jobbe sammen. Dette bidro til å gi alle en større forståelse for alles aspekter av prosjektet, slik at ikke alle kun hadde fokus på egne felt og arbeidområder. Det gjorde det også enklere for hverandre å få hjelp og støtte fra de andre i gruppen. 

Vi har vært en relativt stor gruppe, med 6 medlemmer. Dette har ført til at det tidhvis har vært vanskelig å fordele enkelt oppgaver, da det ofte har vært fler mennesker enn prioriterte oppgaver. VI følte likevel vi klarte å få relativt god kontroll på dette ettehvert, mye takket være bruk av verktøy som trello for å organisere arbeidsoppgaver.

## Prosjektverktøy 
Vi bruker trello til kanban. Også bruker vi Libgdx og Box2d for spillimplementasjon. Vi bruker Tiled for å lage mappet.

Her er en lenke til trello boardet som vi har brukt.
https://trello.com/b/NoTT8KFC/h%C3%B8ytek-survivors

## Hvordan fungerer gruppedynamikk og kommunikasjon?


Gruppen kommer godt overens og mye tid blir brukt på å drøfte forskjellige ideer og implementasjoner til prosjektet. Vi møtes fysisk 1-2 ganger i uken, men er også aktive på messenger og discord. Vi føler at alle i gruppen er engasjerte og kommer med gode innspill, noe som fører til et godt arbeidsmiljø. Vi har opplevd gjennomprosjektet at alle på gruppen er ambasiøse og har hatt et stort ønske å få til et godt resultat. Dette gjør at vi motiverer hverandre for å legge in den ekstra innsatsen. 



## Kort retrospektivt om hvordan prosjektet har gått og hva vi ville endret om vi startet på nytt.


Som gruppe er vi stort sett fornøyde med hvordan vi har jobbet oss gjennom dette prosjektet. Vi har hatt god arbeidsfordeling og har jobbet bra sammen. De små utfordringene vi har møtt på på veien, syntes vi at vi har klart å håndtere på en god måte. 

Vi har klart å holde en relativt jevn flyt gjennom prosjektet, og har sluppet de aller største skippertakene før tidsfristene har kommet. 

Til tross for mange positive aspekter, er det rom for forbedringer. Som nevnt tidligere, ble vårt største ankepunkt mangel på tidlig og kontinuerlig testing. Dette skyldtes delvis vår tidlige fokus på utvikling av spilllogikk og grafikk, noe som resulterte i at vi undervurderte viktigheten av gode testrutiner. Tilabekemldignene vi mottok gjenspeilet også dette. Når vi mot slutten av prosjektet begynte å teste mer intensivt, oppdaget vi flere feil og problemer som kunne ha blitt identifisert og adressert tidligere. Dette medførte at vi måtte restrukturere betydelige deler av koden vår, hovedsakelig ved å skille logikk og grafikk i alle klassene våre. Dette var en tidkrevende prosess, og noe vi kunne ha unngått dersom vi hadde testet mer kontinuerlig fra start av.

I begynnelsen av prosjektet hadde kanskje litt for høye ambisjoner om hva programmet vårt skulle inneholde. Denne entusiasmen fikk oss til å stadig legge til nye funksjoner og implementeringer, fremfor å forbedre og strukturere det vi allerede hadde utviklet. Resultatet var at vi ofte måtte gå tilbake og gjøre endringer på tidligere arbeid, noe som kunne vært unngått med en mer planmessig tilnærming fra starten av.

Etter hvert som prosjektet skred frem, innså vi at vi måtte justere ned ambisjonsnivået. Vi erkjente at tiden ikke strakk til for å realisere alle våre ønsker samtidig som vi opprettholdt høy kodekvalitet og solid testdekning. Dette ble en verdifull lærdom for oss. Erfaringene fra dette prosjektet har gitt oss innsikt i viktigheten av balansen mellom ambisjon og gjennomførbarhet, noe vi definitivt vil ta med oss videre i fremtidige prosjekter

I startfasen av prosjektet opplevde vi en viss ubalanse i arbeidsfordelingen blant gruppemedlemmene. Dette skyldtes at vi ha ulike fag og ulike timeplane, noe som resulterte i at arbeidet med prosjektet ble prioritert forskjellig fra person til person. Etterhvert som vi kom lenger inn i prosjektet, merket vi at det var betydelige forskjeller i vår forståelse av koden og vårt arbeidsverktøy, libGDX. Dette førte til at ikke alle medlemmene var på samme nivå, noe som skapte utfordringer. Noen medlemmer måtte jobbe hardt for å ta igjen det tapte, mens de mer erfarne fortsatte å utvikle spillet. Disse utfordringene er omtalt i våre møtereferater og i oblig 2. Vi følte imidlertid at vi klarte å håndtere disse problemene relativt greit, og alle medlemmene føler nå at de har bidratt godt til gruppearbeidet.


## Krav og spesifikasjoner

Siden sist har vi jobbet mye med tester. Vi hadde store problemer med å teste klassene som bruker mye funksjonalitet fra libGDX, men vi ble senere informert om at dette ikke var nødvendig. Vi brukte også mye til og energi på å refaktorere deler av den eksisterende koden.

Siden Oblig 3 har vi:

- Ferdigstillt upgrade-menyen med ny grafikk og funksjonalitet.

- Lagt til en Victory Screen etter fem waves er overvunnet. Vi fålte dette ville gi en mer helhetlig    spillopplevelse, selv om du kan fortsette spillet etter Victory-skjermen.

- Vi la også inn en Game over screen når man taper spillet, her kan man gå tilbake til startmenyen eller avslutte spillet.

- Vi lagde et system med power-ups som spilleren kan plukke opp for å få midlertidige stat-boosts (speed og damage).

- Vi la også inn en ny enemy-type, som vi kreativt døpte "BlueEnemy". Denne fienden har mer liv enn RedEnemy.

- Vi utarbeidet et wave-system som spawner enemies og trigger upgrade-menyen.

- Vi har også lagt til flere lydeffekter/musikk, og oppdatert de eksisterende.


## Minimum Viable Product (MVP)
Her er vår MVP, laget på starten av prosjektet:

1. Spilleren får presentert en forside med en stor overskrift og musikk spilles for å skape en stemning. Samtidig finnes det en knapp for instruksjoner og å starte spillet. 

2. Når spilleren trykker på spill viser skjermen et spillbrett hvor spillerens karakater står i midten.

3. Spilleren kan flytte karakteren ved hjelp av tastaturet.

4. Spilleren kan bevege seg med terrenget som har effekter på karakteren.

5. Fiender blir konstruert utenfor skjermen og beveger seg mot spilleren.

6. Spilleren kan angripe fiender og fiender kan angripe spilleren.

7. Spilleren kan plukke opp power ups som gir spilleren en kortvarig fordel.

7. Spilleren får poeng for å drepe fiender.

8. Spilleren har en gitt mengde liv og mister liv når fiender angriper/kommer i kontakt med spilleren.

9. Etter ett antall fiender er drept skal spilleren få en meny som viser ulike oppgraderinger spilleren kan velge mellom.

10. Når spilleren mister alle livene sine, vises en skjerm som viser at spillet er over og en mulighet for å starte spillet på nytt.

I den endelige versjonen av spillet har vi oppfyllt alle krav til MVP med unntak av punkt 7. Vi følte at et score-system bare var av verdi dersom vi utarbeidet et system rundt det, noe vi valgte å avprioritere.



## Brukerhistorie, akseptansekriterier og arbeidsoppgaver
Her er målene etter oblig 3. Endelig kommentar til hvert punkt er markert med "#".


1. Waves av enemies som representerer hver runde
- Akseptanserkriterier
    - Spilleren skal møte en ny wave av enemies etter at alle enemies i forrige wave er drept
    - Antall enemies i hver wave skal øke for hver runde
    - Enemies skal ha en økende vanskelighetsgrad for hver runde
- Arbeidoppaver
    - Lage en metode som teller antall drepte fiender.
    - Når disse fiendene er drept, skal en ny wave av enemies spawne
    - Endre enemies sine egenskaper for hver wave.
    #her valgte vi å gi hver wave en konstant økende mengde enemies. Enemies får også mer heath som en funksjon av wave-nummeret, ellers er alt implementert.

2. Oppgraderingsmeny
- Akseptanserkriterer
   - Spilleren skal mellom hver wave, få opp en meny som viser ulike oppgraderinger
   - Oppgraderinsmenyen kan brukes av spilleren for å få permanente oppgraderinger
    - Oppgraderingene skal kunne forbedre spillerens egenskaper, som for eksempel angrepskraft, helse og hastighet.
- Arbeidsoppgaver
    - Lage en metode som kan kalles på fra UpgradeScreen som oppgraderer spillerens egenskaper
    - Lage en metode som oppdaterer spillerens egenskaper i henhold til oppgraderingene
    - Lage en metode som åpner upgrademenyen basert på waves.
    #her er alt oppfyllt, vi la også inn noen ekstra oppgraderinger som health regen og attack speed (cooldown reduction).

3. Enemies skal kunne ta liv av spilleren.
- Akseptanserkriterier
    - Spilleren skal miste liv dersom han blir truffet av en enemy
    - Spilleren skal kunne dø dersom han mister alle livene sine

- Arbeidsoppgaver
    - Lage en metode som sjekker om spilleren har kollidert med en enemy
    - Metoden skal trekke fra liv fra spilleren dersom han har kollidert med en enemy
    - Metoden skal sjekke om spilleren har mistet alle livene sine

    #Her er alt oppfylt, vi la også inn en begrensning slik at spilleren bare kan ta en instance av damage av gangen, ellers ble spillet altfor vanskelig.

## Bugs
- diagonal bevegelse går raskere en horisonatal og vertikal bevegelse. Dersom du beveger deg skått, altså trykker "w" og "d" samtidig, vil du bevege deg raskere enn om du trykker "w" eller "d" alene.


## Forbedringer

Vi er veldig fornøyde med hvordan spillet har utviklet seg så langt, men har også en del forbedringer som vi gjerne skulle forbedret. Blant annet vil vi gjerne utvikle spillbalansen ettersom man kommer videre i spillet. Dette vil si å forbedre forholdet mellom spilleren og fiendene, slik at spillet blir mer utfordrende etterhvert som kommer lengre i spillet. Vi har dette på plass for de første nivåene, men det blir mer og mer ubalansert lenge ut. 

Vi kunne lagd til flere funskjoner for våpen til både spilleren og enemies. Dette kunne vært gjort ved å legge til flere powerups som gir spilleren forskjellige våpen, som for eksempel en shotgun eller en laserpistol. Dette kunne også vært lagt til for enemies, slik at de kunne hatt forskjellige våpen og angrepsmetoder.

Vi kunne også lagt til flere forskjellige fiender, som har forskjellige egenskaper og angrepsmetoder. Dette kunne gjort spillet mer variert og interessant for spilleren. Det kan bli lagt til "bosser" som er vanskeligere å drepe enn vanlige enemies.



## Styring av spillkarakter
- Spillkarakteren styres ved bruk av 'w', 'a', 's' og 'd'
- Spilleren skyter fireballs automatisk i retningen av crosshair

## Produkt og kode:

## Dette har vi fikset siden sist: oblig4 spesikt:

- Lagd AbstractScreen for alle screenklasser.
- Upgrade meny som kommer opp mellom hver wave.
- Blue enemy som har mer liv enn Red enemy.
- Forbedret MVC oppsett.
- Fullført testingen av programmet
- Lagt til screens for ulike hendelser i spillet.
- Restrukturert på managerklassene.

## Dette ville vi gjort annerledes:

- Vi ville ha testet mer kontinuerlig gjennom hele prosjektet.
- En tydeligere Model-View-Controller struktur fra starten av.
- Bedre planlegging av arbeidsoppgaver og fordeling av disse.
- Bedre bruk av verktøy som Trello for å organisere arbeidsoppgaver.

## Testing

Vi har valgt å skille view delen fra model og kontroller slik at vi kan teste de logiske metodene best mulig. Derfor endte vi på 68% test coverage over alle metoder i prosjektet. Om man ser bort fra view klassene og spessielt PlayScreen.java ligger vi på godt over 75% og vi har nesten 100% på de viktige metodene og klassene som ligger i model og tools. 

GameCreate, Hud, og Main er også vanskleige å teste fordi man får en feil "error compiling shader". Denne greide vi ikke å få vekk uansett hvor mye vi prøvde å mocke textures, opengl og ved bruk av headless aplication. 

Om man ser bortifra view helt har vi 117/131 metoder testet eller 89%, vi har fått testet enkelte klasses i view, så det kan være interresant å gå over de også. 

## Kodestruktur

Vi har valgt å benytte oss av en MVC struktur. Hver entitet i spillet blir laget i factories untatt PlayerModel, dette fordi det er kun en instans av player'en. Vi har restrukturert koden til å benytte Managers, disse er ment for å adskille instansiering av entiteter og oppdateringer på dem ila game loopen. Hver entitet som er collideble (kan kollidere med andre entiteter) har fått en collisionhandler til seg. Dette har vi valgt for å gjøre det enklere å kontrollere kollison, samt ved brukt av SOLID prinsippet der klassene skal gjøre mest mulig singulære ting. Vi prøvde å samle alt i en klasse, men det ble rotete, dermed var SOLID den beste løsningen. De fleste av klassene samles i PlayScreen.java. Denne klassen holder styr på alle entiter, samt der main game loop blir håndert. I update() og render() blir logikken og visningen oppdatert etterhvert som tiden går. Med den nye strukturen vår har det blitt enklere å implementere nye features samt å fikse bugs som vi hadde tidligere.
## Klassediagram
![Klassediagram](klassediagram4.png)

## Referater

Referater fra møter ligger vedlagt i mappen "referater". Innholdet i referatene gjenspeiles godt gjennom rapportene. 










