Referat 25.04.2024

Oppmøte: Magnus, Jens, Brage, Lyder, Johannes og Henrik. 

Framgang side sist: Lagt til blueEnemy. testet alle set og getters.

Vi må restrukturere koden vår for å opti alisere testing. Vi har vært for dårlig på å teste underveis. Det er per nå nesten umulig å teste ting i enkelte klasser. Må spilette opp alle klassene i Model-view-controller. Viktig å skille ut view, da det er vanskelig å teste grafikken. 

Dette må gjøres for enemy, fireball, og powerup. Har allerede gjort for player, så kan bruke dette som mal. 

