# Høytek Nightfall: Survivors Saga Deluxe Edition

## Team
* Brage Hogstad
* Henrik Tennebekk
* Jens Brown Eriksen
* Johannes Helleve
* Lyder Samnøy
* Magnus Fjeldstad

## About the Game
Høytek Nightfall Survivors is an action-packed survival game that challenges players to combat hordes of enemies on increasingly challenging levels. Players gradually upgrade their characters between levels, making them stronger for each round. The player shoots a cone of fireballs, aiming to hit the enemies and kill them. The player can pick up powerups such as speed and damage to gain a temporary advantage. At the end of each wave, the player will be able to buy an upgrade to improve the stats of their enemy. The game ends when the player dies or completes wave five. The player is however able to continue after wave five if the player wants to. If the player contiues, the waves will get stronger and stronger. The game then ends when the player dies. The goal will then be to complete the most waves.

## Running
Compile with `mvn package`.
Run by using the command `mvn exec:java`.
Requires Java 21 or later.

## Known Issues
The player has a higher speed when moving diagonally. This is because diagonal movementuses full speed on both the X and Y axis.

## Controls
W - Up
S - Down
A - Left
D - Right

Keypad/Mouse - Aim

## Credits/3rd party resources

### Images

##### Player and enemies
Player_and_enemy.png - (Open Gameart, https://opengameart.org/content/rpg-character)

rougelikecreatures.png - (@JoeCreates on X, https://opengameart.org/content/roguelike-monsters)

*RedEnemy.png and BlueEnemy.png are both derived from rougelikecreatures.png

##### Powerups and Weapons
boots.png - (Freepik, https://www.freepik.com/icon/boots_14321652#fromView=search&page=1&position=51&uuid=c89af030-8515-489b-a158-2dd2a8094620)

muscle.png - (Vecteezy, https://www.vecteezy.com/vector-art/21706121-hand-muscle-sign-in-pixel-art-style)

fireball.png - (Master484, https://opengameart.org/content/explosion-set-1-m484-games)

##### Map and Powerups
tileset_gutter.png - (Spriters Resource, "https://www.spriters-resource.com/fullview/52571/") 

##### Background and UI
gameover.png - (Dall-e)

pausebackground.png - (Dall-e)

mainmenubackground.png - (Dall-e)

upgradescreenbackground.png - (Dall-e)

victorybackground.png - (Dall-e)

pixthulhu-ui.png - (https://ray3k.wordpress.com/pixthulhu-ui-skin-for-libgdx/)

credits.png - Made in Microsoft Word with font AudioWide

instruksjon.png - Made in Microsoft Word with font AudioWide

gameover.png - Made in Microsoft Word with font AudioWide

### Sounds
theme.wav - (Magnus J. Tennøe)

