package inf112.skeleton.app;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

public class Main {
    public static void main(String[] args) {
        Lwjgl3ApplicationConfiguration cfg = new Lwjgl3ApplicationConfiguration();
        cfg.setForegroundFPS(60);
		cfg.setTitle("HØYTEK: NIGHTFALL SURVIVORS");
        cfg.setWindowedMode(1920, 1080);
        new Lwjgl3Application(new GameCreate(), cfg);
    }
}