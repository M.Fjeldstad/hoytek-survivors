package inf112.skeleton.app.view.weaponsView.fireballView;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.model.weapons.fireball.Fireball;

public class FireballView extends Sprite {

    private final TextureRegion textureRegionFireball;
    private final Texture textureFireball;

    protected Fireball fireball;

    public FireballView(Fireball fireball) {
        this.fireball = fireball;
        this.textureFireball = new Texture("weapons/fireball/fireball.png");
        this.textureRegionFireball = new TextureRegion(textureFireball);
        initializeSprite();
    }

    /**
     * Initializes the sprite
     */
    protected void initializeSprite() {
        float scalingFactor = 0.08f;
        setBounds(0, 0, textureFireball.getWidth() / GameCreate.PPM * scalingFactor,
                textureFireball.getHeight() / GameCreate.PPM * scalingFactor);

        setRegion(textureRegionFireball);
    }

    /**
     * Updates the spirts position
     * 
     * @param dt time since last frame
     */
    public void update(float dt) {
        float x = fireball.getPosition().x - getWidth() / 2;
        float y = fireball.getPosition().y - getHeight() / 2;
        setPosition(x, y);
    }


    /**
     * @return fireball
     */
    public Fireball getFireball() {
        return fireball;
    }

    /**
     * Disposes of texture
     */
    public void dispose() {
        textureFireball.dispose();
    }
}
