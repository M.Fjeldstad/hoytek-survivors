package inf112.skeleton.app.view.powerupView;

import com.badlogic.gdx.graphics.Texture;

import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.model.powerups.AbstractPowerUp;

public class DamagePowerUpView extends AbstractPowerUpView {
    /**
     * Constructor for DamagePowerUpView
     * @param powerUp takes in a powerup
     */
    public DamagePowerUpView(AbstractPowerUp powerUp) {
        super(powerUp, "powerups/muscle.png");
        setScale(0.1f);
    }

    /**
     * !!!ONLY FOR TESTING!!!
     * 
     * Alternative constructor for DamagePowerUpView 
     * @param powerUp takes in a powerup
     * @param texture takes in a texture
     */
    public DamagePowerUpView(AbstractPowerUp powerUp, Texture texture) { 
        super(powerUp, texture);
    }

    @Override
    protected void initializeSprite() {
        super.initializeSprite();
        setScale(0.05f);
        float x = (powerUp.getPosition().x - getWidth() *4 ) / GameCreate.PPM;
        float y = (powerUp.getPosition().y - getHeight() *4) / GameCreate.PPM;
        setPosition(x, y);
    }
}
