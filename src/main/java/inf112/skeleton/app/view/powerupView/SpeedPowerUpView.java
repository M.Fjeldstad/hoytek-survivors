package inf112.skeleton.app.view.powerupView;

import com.badlogic.gdx.graphics.Texture;

import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.model.powerups.AbstractPowerUp;

public class SpeedPowerUpView extends AbstractPowerUpView {
    /**
     * Constructor for SpeedPowerUpView
     * @param powerUp takes in a powerup
     */
    public SpeedPowerUpView(AbstractPowerUp powerUp) {
        super(powerUp, "powerups/boots.png");
    }

    /**
     * !!!ONLY FOR TESTING!!!
     * 
     * Alternative constructor for SpeedPowerUpView 
     * @param powerUp takes in a powerup
     * @param texture takes in a texture
     */
    public SpeedPowerUpView(AbstractPowerUp powerUp, Texture texture) { // Ny konstruktør for testing. 
        super(powerUp, texture);
    }

    
    @Override
    protected void initializeSprite() {
        super.initializeSprite();
        setScale(0.05f);
        float x = (powerUp.getPosition().x - getWidth()) / GameCreate.PPM;
        float y = (powerUp.getPosition().y - getHeight()) / GameCreate.PPM;
        setPosition(x, y);
    }
}