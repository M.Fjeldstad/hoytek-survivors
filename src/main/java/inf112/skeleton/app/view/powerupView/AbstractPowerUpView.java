package inf112.skeleton.app.view.powerupView;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.model.powerups.AbstractPowerUp;

public abstract class AbstractPowerUpView extends Sprite {
    protected AbstractPowerUp powerUp;
    private Texture texturePowerUp;

    /**
     * 
     * @param powerUp takes in a powerup
     * @param texturePath takes in a texture path
     */
    public AbstractPowerUpView(AbstractPowerUp powerUp, String texturePath) {
        this.powerUp = powerUp;
        this.texturePowerUp = new Texture(texturePath);
        initializeSprite();
    }

    /**
     * !!!ONLY FOR TESTING!!!
     * 
     * Alternative constructor for AbstractPowerUpView 
     * @param powerUp takes in a powerup
     * @param texture takes in a texture
     */
    public AbstractPowerUpView(AbstractPowerUp powerUp, Texture texture) {
        this.powerUp = powerUp;
        this.texturePowerUp = texture;
        initializeSprite();
    }

    /**
     * Initializes the sprite
     */
    protected void initializeSprite() {
        setRegion(new TextureRegion(texturePowerUp));
        float width = texturePowerUp.getWidth() / GameCreate.PPM;
        float height = texturePowerUp.getHeight() / GameCreate.PPM;
        setSize(width, height);
        setScale(0.1f); 
    }

    /*
     * @return powerUp texture
     */
    public Texture getTexture() {
        return texturePowerUp;
    }
    
    /**
     * 
     * @return the powerup
     */
    public AbstractPowerUp getPowerUp() {
        return powerUp;
    }

    /**
     * disposes the powerup
     */
    public void dispose() {
        texturePowerUp.dispose();
    }
}
