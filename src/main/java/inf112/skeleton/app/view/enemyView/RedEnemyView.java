package inf112.skeleton.app.view.enemyView;

import inf112.skeleton.app.model.enemies.AbstractEnemy;

public class RedEnemyView extends AbstractEnemyView {

    /**
     * Constructor for BlueEnemyView
     * 
     * @param enemy
     */
    public RedEnemyView(AbstractEnemy enemy) {
        super(enemy, "enemy/RedEnemy.png");
    }
}
