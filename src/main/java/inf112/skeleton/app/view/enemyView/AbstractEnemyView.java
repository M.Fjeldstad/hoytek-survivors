package inf112.skeleton.app.view.enemyView;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.model.enemies.AbstractEnemy;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class AbstractEnemyView extends Sprite {

    private final TextureRegion textureRegionEnemy;
    private final Texture textureEnemy;

    protected AbstractEnemy enemy;

    /**
     * Constructor for AbstractEnemyView
     * 
     * @param enemy
     * @param texturePath
     */
    public AbstractEnemyView(AbstractEnemy enemy, String texturePath) {
        this.enemy = enemy;
        this.textureEnemy = new Texture(texturePath);
        this.textureRegionEnemy = new TextureRegion(textureEnemy);

        initializeSprite();
    }

    /**
     * Updates AbstractEnemyView
     * 
     * @param deltaTime
     */
    public void update(float deltaTime) {
        float spriteWidth = getWidth();
        float spriteHeight = getHeight();
        float x = enemy.getPosition().x - spriteWidth / 2;
        float y = enemy.getPosition().y - spriteHeight / 2;
        setPosition(x, y);
    }

    /**
     * @return enemy
     */
    public AbstractEnemy getEnemy() {
        return enemy;
    }

    /**
     * Initializes the sprite
     */
    protected void initializeSprite() {
        setRegion(textureRegionEnemy);
        setSize(16/ GameCreate.PPM, 16 / GameCreate.PPM);
    }

    /**
     * Disposes of texture
     */
    public void dispose() {
        textureEnemy.dispose();
    } 
}
