package inf112.skeleton.app.view.enemyView;

import inf112.skeleton.app.model.enemies.AbstractEnemy;

public class BlueEnemyView extends AbstractEnemyView {

    /**
     * Constructor for BlueEnemyView
     * 
     * @param enemy
     */
    public BlueEnemyView(AbstractEnemy enemy) {
        super(enemy, "enemy/BlueEnemy.png");
    }
}