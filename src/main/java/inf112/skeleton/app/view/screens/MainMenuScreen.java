package inf112.skeleton.app.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import inf112.skeleton.app.tools.sound.SoundManager;
import inf112.skeleton.app.GameCreate;

public class MainMenuScreen extends AbstractScreen {

    // Texture for the background image
    private final Texture backgroundTexture;

    // Image for the background image
    private Image backgroundImage;

    // SoundManager for the theme music
    private final SoundManager soundManager;

    /**
     * Constructor for the MainMenuScreen
     * 
     * @param game
     */
    public MainMenuScreen(GameCreate game) {
        super(game);

        backgroundTexture = new Texture(Gdx.files.internal("bkgImage/mainmenubackground.png"));
        
        // Initiates the theme music
        soundManager = new SoundManager();
        soundManager.themeMusic.getSound().setLooping(true);
        soundManager.themeMusic.play();

        createLayout();
    }

    /**
     * Creates the layout of the MainMenuScreen
     */
    protected void createLayout() {
        // Create a table to hold the buttons
        Table table = new Table();
        table.setFillParent(true); // Make the table fill the stage

        // Setting the background image of the screen
        backgroundImage = new Image(backgroundTexture);
        backgroundImage.setFillParent(true);

        // Adds buttons to the table
        TextButton startButton = new TextButton("Start", skin);
        TextButton instructionsButton = new TextButton("How To Play", skin);
        TextButton creditsButton = new TextButton("Credits", skin);
        TextButton quitButton = new TextButton("Quit", skin);

        // Button to start a new game
        startButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new PlayScreen(game));
            }
        });

        instructionsButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new InstructionScreen(game));
            }
        });

        creditsButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new CreditsScreen(game));
            }
        });

        quitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });

        // Add buttons to the table with padding, then move to the next row
        table.bottom().padBottom(40);
        table.add(startButton).pad(8).row();
        table.add(instructionsButton).pad(8).row();
        table.add(creditsButton).pad(8).row();
        table.add(quitButton).pad(8).row();

        // Add the background image and the table to the stage
        stage.addActor(backgroundImage);
        stage.addActor(table);

    }

    @Override
    public void hide() {
        // Stop the theme music when the screen is hidden
        soundManager.themeMusic.stop();
    }
}