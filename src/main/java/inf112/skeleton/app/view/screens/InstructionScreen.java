package inf112.skeleton.app.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import inf112.skeleton.app.GameCreate;

public class InstructionScreen extends AbstractScreen {

    // Texture for the instruction image
    private final Texture instructionTexture;

    // Image for the instruction image
    private Image instructionImage;

    /**
     * Constructor for the InstructionScreen
     * 
     * @param game
     */
    public InstructionScreen(GameCreate game) {
        super(game);

        instructionTexture = new Texture(Gdx.files.internal("bkgImage/instruksjon.png"));

        createLayout();

    }

    /**
     * Creates the layout of the InstructionScreen
     */
    protected void createLayout() {
        // Create a table to hold the buttons
        Table table = new Table();
        table.setFillParent(true); // Make the table fill the stage
        
        // Setting the instruction image to the screen
        instructionImage = new Image(instructionTexture);
        instructionImage.setFillParent(true);
    
        // Add button to the table
        TextButton backButton = new TextButton("Back", skin);
    
        // Back button to return to main menu
        backButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new MainMenuScreen(game));
            }
        });
    
        // Add the button to the table with padding
        table.add().spaceRight(1175);
        table.add(backButton).padBottom(-650);
    
        // Add the instruction image and the table to the stage
        stage.addActor(instructionImage);
        stage.addActor(table);

    }
}
