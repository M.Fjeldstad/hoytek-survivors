package inf112.skeleton.app.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import inf112.skeleton.app.GameCreate;

public class GameOverScreen extends AbstractScreen {
    
    // Texture for the game over image
    private final Texture gameOverTexture;
    // Image for the game over image
    private Image image;

    /**
     * Constructor for the GameOverScreen
     * 
     * @param game
     */
    public GameOverScreen(GameCreate game) {
        super(game);
        gameOverTexture = new Texture(Gdx.files.internal("bkgImage/gameover.png"));
        createLayout();
    }

    /**
     * Creates the layout of the GameOverScreen
     */
    protected void createLayout() {
        // Create a table to hold the buttons
        Table table = new Table();
        table.setFillParent(true); // Make the table fill the stage
        
        // Setting the game over image to the screen
        image = new Image(gameOverTexture);
        image.setFillParent(true);

        // Add button to the table
        TextButton mainMenuButton = new TextButton("Main Menu", skin);
        TextButton quitButton = new TextButton("Quit", skin);

        // Back button to return to main menu
        mainMenuButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new MainMenuScreen(game));
            }
        });

        // Quit button to close the game
        quitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });

        // Add the buttons to the table with padding
        table.add(mainMenuButton).padBottom(-600).spaceRight(500);
        table.add(quitButton).padBottom(-600).spaceLeft(500);

        // Add the game over image and the table to the stage
        stage.addActor(image);
        stage.addActor(table);

    }
}
