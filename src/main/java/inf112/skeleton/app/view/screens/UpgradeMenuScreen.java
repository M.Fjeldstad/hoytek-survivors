package inf112.skeleton.app.view.screens;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;

import inf112.skeleton.app.model.weapons.fireball.*;

import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.model.player.PlayerModel;

public class UpgradeMenuScreen extends AbstractScreen {

    // FireballManager to adjust fireball spawn interval
    private FireballManager fireballManager;

    // Texture for the upgrade menu background
    private final Texture upgradeMenuBackground;

    // Image for the upgrade menu background
    private Image upgradeMenuBackgroundImage;

    /**
     * Constructor for the UpgradeMenuScreen
     * 
     * @param game
     * @param playerModel
     * @param playScreen
     */
    public UpgradeMenuScreen(GameCreate game, PlayerModel playerModel, PlayScreen playScreen) {
        super(game, playScreen, playerModel);
        this.fireballManager = playScreen.getFireballManager();

        upgradeMenuBackground = new Texture(Gdx.files.internal("bkgImage/upgradescreenbackground.png"));

        createLayout();
    }

    /**
     * Creates the layout of the UpgradeMenuScreen
     */
    protected void createLayout() {
        // Create a table to hold the buttons
        Table table = new Table();
        table.setFillParent(true); // Make the table fill the stage

        upgradeMenuBackgroundImage = new Image(upgradeMenuBackground);
        upgradeMenuBackgroundImage.setFillParent(true);

        // Add buttons to adjust upgrades
        TextButton healthUpgradeButton = new TextButton("Upgrade Health", skin);
        healthUpgradeButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                playerModel.addMaxHealth(20);
                playerModel.addHealth(20);
                game.setScreen(playScreen);
            }
        });

        // Add buttons to adjust upgrades
        TextButton DamageUpgradeButton = new TextButton("Upgrade damage", skin);
        DamageUpgradeButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                playerModel.addAttackDamage(5);
                game.setScreen(playScreen);
            }
        });

        // Add health regeneration upgrade button
        TextButton healthRegenUpgradeButton = new TextButton("Upgrade Health Regen", skin);
        healthRegenUpgradeButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                // Increment health regeneration by 1
                playerModel.setHealthRegen(playerModel.getHealthRegen() + 1);
                game.setScreen(playScreen);
            }
        });

        // Add movement speed upgrade button
        TextButton speedUpgradeButton = new TextButton("Upgrade Speed", skin);
        speedUpgradeButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                playerModel.addMaxSpeed(.5f);
                playerModel.addSpeed(.5f);
                game.setScreen(playScreen);
            }
        });
        TextButton spawnIntervalUpgradeButton = new TextButton("Upgrade attack Speed", skin);
        spawnIntervalUpgradeButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (fireballManager != null) { // Check if fireballManager is initialized
                    // Reduce spawn interval by 20% for each upgrade
                    float currentSpawnInterval = fireballManager.getSpawnInterval();
                    float newSpawnInterval = currentSpawnInterval * 0.80f; // 20% reduction
                    fireballManager.setSpawnInterval(newSpawnInterval);
                    game.setScreen(playScreen);
                }
            }
        });

        // Add the buttons to the table with padding
        table.add(healthUpgradeButton).pad(5).row();
        table.add(healthRegenUpgradeButton).pad(5).row();
        table.add(DamageUpgradeButton).pad(5).row();
        table.add(speedUpgradeButton).pad(5).row();
        table.add(spawnIntervalUpgradeButton).pad(5).row();

        // Add the upgrade menu background image and the table to the stage
        stage.addActor(upgradeMenuBackgroundImage);
        stage.addActor(table);

    }
}
