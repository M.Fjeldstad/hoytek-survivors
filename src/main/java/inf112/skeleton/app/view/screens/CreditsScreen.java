package inf112.skeleton.app.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import inf112.skeleton.app.GameCreate;


public class CreditsScreen extends AbstractScreen{
    
    // Texture for the credits image
    private final Texture creditsTexture;
    
    // Image for the credits image
    private Image creditsImage;
    
    /**
     * Constructor for the CreditsScreen
     * 
     * @param game
     */
    public CreditsScreen(GameCreate game){
        super(game);
        creditsTexture = new Texture(Gdx.files.internal("bkgImage/credits.png"));
        createLayout();
    }

    /**
     * Creates the layout of the CreditsScreen
     */
    protected void createLayout() {
        // Create a table to hold the buttons
        Table table = new Table();
        table.setFillParent(true); // Make the table fill the stage
        
        // Setting the credits image to the screen
        creditsImage = new Image(creditsTexture);
        creditsImage.setFillParent(true);

        // Add button to the table
        TextButton backButton = new TextButton("Back", skin);
        
        // Back button to return to main menu
        backButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new MainMenuScreen(game));
            }
        });
        
        // Add the button to the table with padding
        table.add().spaceRight(1175);
        table.add(backButton).padBottom(-650);
        
        // Add the credits image and the table to the stage
        stage.addActor(creditsImage);
        stage.addActor(table);
    }
}
