package inf112.skeleton.app.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.model.player.PlayerModel;

// Abstract class for screens used in the game
public abstract class AbstractScreen extends ScreenAdapter {
    protected final GameCreate game;
    protected Stage stage;
    protected Skin skin;
    protected OrthographicCamera camera;
    protected Viewport viewport;
    protected PlayScreen playScreen;
    protected PlayerModel playerModel;


    /**
     * Constructor for screens that only need a GameCreate object.
     *
     * @param game the GameCreate object
     */
    public AbstractScreen(GameCreate game) {
        this.game = game;
        initialize();
    }

    /**
     * Constructor for screens that need both a GameCreate object and a PlayScreen object.
     *
     * @param game the GameCreate object
     * @param playScreen the PlayScreen object
     */
    public AbstractScreen(GameCreate game, PlayScreen playScreen) {
        this.game = game;
        this.playScreen = playScreen;
        initialize();
    }

    /**
     * Constructor for screens that need a GameCreate object, a PlayScreen object, and a PlayerModel object.
     *
     * @param game the GameCreate object
     * @param playScreen the PlayScreen object
     * @param playerModel the PlayerModel object
     */
    public AbstractScreen(GameCreate game, PlayScreen playScreen, PlayerModel playerModel) {
        this.game = game;
        this.playScreen = playScreen;
        this.playerModel = playerModel;
        initialize();
    }

    /** 
     * Initializations for all of the screens which extends AbstractScreen
     */ 
    private void initialize() {
        camera = new OrthographicCamera();
        
        viewport = new ScreenViewport(camera);
        viewport.apply();

        stage = new Stage(viewport, game.getBatch());
        Gdx.input.setInputProcessor(stage);

        loadAssets();
    }

    /**
     * Load the assets needed for the screen.
     */
    protected void loadAssets() {
        TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("pixthulhu-ui.atlas"));
        skin = new Skin(Gdx.files.internal("pixthulhu-ui.json"), atlas);
    }

    protected abstract void createLayout();

    
    /**
     * Called when this screen becomes the current screen for a Game.
     * Sets the input processor to the stage.
     */
    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    /**
     * Called when the screen should render itself.
     * Clears the screen, updates the stage, and draws the stage.
     *
     * @param delta the time in seconds since the last render
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    /**
     * Called when the Application is resized.
     * Updates the viewport and camera.
     *
     * @param width the new width in pixels
     * @param height the new height in pixels
     */
    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();
    }

    /**
     * Called when this screen is no longer the current screen for a Game.
     * Sets the input processor to null.
     */
    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    /**
     * Called when this screen should release all resources.
     * Disposes the stage and the skin.
     */
    @Override
    public void dispose() {
        stage.dispose();
        if (skin != null) {
            skin.dispose();
        }
    }

    /**
     * @return the viewport associated with this screen for testing or other purposes.
     */
    public Viewport getViewport() {
        return viewport;
    }
}
