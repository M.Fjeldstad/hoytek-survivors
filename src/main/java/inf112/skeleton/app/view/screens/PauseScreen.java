package inf112.skeleton.app.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import inf112.skeleton.app.GameCreate;

public class PauseScreen extends AbstractScreen {

    // Texture for the pause image
    private final Texture pauseTexture;

    // Image for the pause image
    private Image pauseImage;


    /**
     * Constructor for the PauseScreen
     * @param game
     * @param playScreen
     */
    public PauseScreen(GameCreate game, PlayScreen playScreen) {
        super(game, playScreen);
        
        pauseTexture = new Texture(Gdx.files.internal("bkgImage/pausebackground.png"));

        createLayout();
    }
    
    /**
     * Creates the layout of the PauseScreen
     */
    protected void createLayout() {
        // Create a table to hold the buttons
       Table table = new Table();
        table.setFillParent(true); // Make the table fill the stage
       
       // Setting the pause image to the screen
        pauseImage = new Image(pauseTexture);
        pauseImage.setFillParent(true);
        

        // Adds buttons to the table
        TextButton resumeButton = new TextButton("Resume", skin);
        TextButton mainMenuButton = new TextButton("Main Menu", skin);
        TextButton quitButton = new TextButton("Quit", skin);

        // Adds listeners to the buttons
        resumeButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(playScreen);
            }
        });

        mainMenuButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new MainMenuScreen(game));
                playScreen.dispose();
            }
        });

        quitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });


        // Add buttons to the table with padding
        table.add(resumeButton).pad(10);
        table.row(); // Move to the next row
        table.add(mainMenuButton).pad(10);
        table.row();
        table.add(quitButton).pad(10);
    
        stage.addActor(pauseImage);
        stage.addActor(table);
    }
}
    

