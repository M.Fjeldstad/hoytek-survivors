package inf112.skeleton.app.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.controller.KeyHandler;
import inf112.skeleton.app.model.enemies.AbstractEnemy;
import inf112.skeleton.app.model.enemies.EnemyEnum;
import inf112.skeleton.app.model.enemies.EnemyManager;
import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.model.powerups.AbstractPowerUp;
import inf112.skeleton.app.model.powerups.PowerUpEnum;
import inf112.skeleton.app.model.powerups.PowerUpManager;
import inf112.skeleton.app.model.weapons.fireball.Fireball;
import inf112.skeleton.app.model.weapons.fireball.FireballManager;
import inf112.skeleton.app.tools.B2WorldCreator;
import inf112.skeleton.app.tools.listeners.PlayerModelCollisionHandler;
import inf112.skeleton.app.tools.listeners.WorldContactListener;
import inf112.skeleton.app.view.enemyView.AbstractEnemyView;
import inf112.skeleton.app.view.enemyView.BlueEnemyView;
import inf112.skeleton.app.view.enemyView.RedEnemyView;
import inf112.skeleton.app.view.playerView.PlayerView;
import inf112.skeleton.app.view.powerupView.AbstractPowerUpView;
import inf112.skeleton.app.view.powerupView.DamagePowerUpView;
import inf112.skeleton.app.view.powerupView.SpeedPowerUpView;
import inf112.skeleton.app.view.scenes.Hud;
import inf112.skeleton.app.view.weaponsView.fireballView.FireballView;

import com.badlogic.gdx.graphics.Cursor.SystemCursor;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.HashMap;
import java.util.Map;

public class PlayScreen extends ScreenAdapter {
    private final TextureAtlas atlas;

    private final GameCreate game;

    // Gamecam + HUD variables
    private OrthographicCamera gamecam;
    private Viewport gamePort;
    private final Hud hud;

    // Map variables
    private final TmxMapLoader mapLoader;
    private final TiledMap map;
    private final OrthogonalTiledMapRenderer renderer;

    // Box2d variabels
    private final World world;
    private final Box2DDebugRenderer b2dr;

    // Sprites
    private final PlayerModel player;
    private final PlayerView playerView;
    private final ShapeRenderer shapeRenderer;

    // Variables for keyhandler
    private final KeyHandler keyHandler;

    // Array of enemies
    private final Array<AbstractEnemy> enemies;

    // Variables for powerups, weapons and enemies
    private final PowerUpManager powerUpManager;
    private final Map<AbstractPowerUp, AbstractPowerUpView> powerUpViews;

    private final FireballManager fireballManager;
    private Map<Fireball, FireballView> fireballViews;

    private final EnemyManager enemyManager;
    private final Map<AbstractEnemy, AbstractEnemyView> enemyViews;

    private final PlayerModelCollisionHandler playerCollisionHandler;

    private final WorldContactListener worldContactListener;

    private int currentWave = 1;
    private boolean gameOver = false;
    private boolean continueGame = false;

    /**
     * Constructor for the PauseScreen
     * 
     * @param game
     */
    public PlayScreen(GameCreate game) {
        atlas = new TextureAtlas("player/Player_and_enemy.atlas");

        this.game = game;
        // Create cam to follow the player
        gamecam = new OrthographicCamera();

        // create a FitViewPort mantain virtual asprect ratio
        gamePort = new FitViewport(GameCreate.V_Width / GameCreate.PPM, GameCreate.V_Height / GameCreate.PPM, gamecam);

        // create our game HUD

        // Map Loader/ MapRenderer
        mapLoader = new TmxMapLoader();
        map = mapLoader.load("map/map1.tmx");
        renderer = new OrthogonalTiledMapRenderer(map, 1 / GameCreate.PPM);
        gamecam.position.set(gamePort.getWorldWidth() / 2, gamePort.getWorldHeight() / 2, 0);

        // Creates the "world" and adds gravity
        world = new World(new Vector2(0, 0), true);

        b2dr = new Box2DDebugRenderer();

        // Creates the B2 world: The physics
        new B2WorldCreator(this);

        // Creates the player
        player = new PlayerModel(this);
        playerView = new PlayerView(this, player);
        shapeRenderer = new ShapeRenderer();

        // Creates a KeyHandler for the player
        keyHandler = new KeyHandler(player, game, this);

        powerUpManager = new PowerUpManager(this, player);
        powerUpViews = new HashMap<>();

        fireballManager = new FireballManager(this);
        fireballViews = new HashMap<>();

        enemyManager = new EnemyManager(this);
        enemyViews = new HashMap<>();

        playerCollisionHandler = player.getPlayerModelCollisionHandler();

        enemies = enemyManager.getEnemies();

        worldContactListener = new WorldContactListener(powerUpManager.getPowerUpCollisionHandler(),
                fireballManager.getFireballCollisionHandler(), enemyManager.getEnemyCollisionHandler(),
                playerCollisionHandler);
        world.setContactListener(worldContactListener);

        hud = new Hud(game.batch);
    }

    /**
     * Updates the game
     * 
     * @param dt is the games "clock" the game updates based on the
     *           deltatime
     */
    public void update(float dt) {

        world.step(1 / 60f, 6, 2);
        keyHandler.handleInput(dt);

        powerUpManager.update(dt);
        checkNewPowerUp();

        fireballManager.update(dt);
        checkNewFireball();

        enemyManager.update(dt);
        checkNewEnemy();

        playerView.update(dt);

        player.update(dt);
        for (AbstractEnemy enemy : enemies) {
            enemy.update(dt);
        }

        // updates the gamecam
        gamecam.position.x = player.getB2body().getPosition().x;
        gamecam.position.y = player.getB2body().getPosition().y;

        setCrosshairCursor();

        gamecam.update();
        renderer.setView(gamecam);

        checksGameOverCondition(dt);
        avalibleUpgrades(dt);
    }

    /**
     * Checks if there are new fireballs added to the game. This is done 
     * because the fireballManager is responsible for creating new fireballs
     * model only. In other words there are no view logic for the fireballs, in the
     * manager. 
     * 
     */
    private void checkNewFireball() {
        for (Fireball fireball : fireballManager.getFireballs()) {
            if (!fireballViews.containsKey(fireball)) {
                fireballViews.put(fireball, new FireballView(fireball));
            }
        }

        for (Fireball fireballToRemove : fireballManager.getFireballsToRemove()) {
            fireballViews.get(fireballToRemove).dispose();
            fireballViews.remove(fireballToRemove);
        }

        fireballManager.getFireballsToRemove().clear();
    }

    /**
     * Same as the one above
     */
    private void checkNewPowerUp() {
        for (AbstractPowerUp powerUp : powerUpManager.getPowerUps()) {
            if (!powerUpViews.containsKey(powerUp) && powerUp.getType() == PowerUpEnum.SPEED_BOOST) {
                powerUpViews.put(powerUp, new SpeedPowerUpView(powerUp));
            }
            if (!powerUpViews.containsKey(powerUp) && powerUp.getType() == PowerUpEnum.DAMAGE_BOOST) {
                powerUpViews.put(powerUp, new DamagePowerUpView(powerUp));
                powerUpViews.putIfAbsent(powerUp, new DamagePowerUpView(powerUp));
            }
        }
        for (AbstractPowerUp powerUpToRemove : powerUpManager.getPowerUpsToRemove()) {
            powerUpViews.get(powerUpToRemove).dispose();
            powerUpViews.remove(powerUpToRemove);
        }
        powerUpManager.getPowerUpsToRemove().clear();
    }

    /**
     * Same as the one above
     */
    private void checkNewEnemy() {
        for (AbstractEnemy enemy : enemyManager.getEnemies()) {
            if (!enemyViews.containsKey(enemy) && enemy.getType() == EnemyEnum.RED_ENEMY) {
                enemyViews.put(enemy, new RedEnemyView(enemy));
            }
            if (!enemyViews.containsKey(enemy) && enemy.getType() == EnemyEnum.BLUE_ENEMY) {
                enemyViews.put(enemy, new BlueEnemyView(enemy));

            }
        }

        for (AbstractEnemy enemyToRemove : enemyManager.getEnemiesToRemove()) {
            enemyViews.get(enemyToRemove).dispose();
            enemyViews.remove(enemyToRemove);
        }

        enemyManager.getEnemiesToRemove().clear();
    }

    @Override
    public void render(float delta) {
        update(delta);

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        renderer.render();

        game.batch.setProjectionMatrix(gamecam.combined);
        game.batch.begin();
        playerView.draw(game.batch);

        for (FireballView fireballView : fireballViews.values()) {
            fireballView.update(delta);
            fireballView.draw(game.batch);
        }

        for (AbstractEnemyView enemyView : enemyViews.values()) {
            enemyView.update(delta);
            enemyView.draw(game.batch);
        }

        for (AbstractPowerUpView powerUpView : powerUpViews.values()) {
            powerUpView.draw(game.batch);
        }

        game.batch.end();

        shapeRenderer.setProjectionMatrix(gamecam.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        playerView.drawHealthBar(shapeRenderer);
        shapeRenderer.end();

        game.batch.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();
    }

    /**
     * Gets the cursors position
     * 
     * @return Vector3
     */
    public Vector3 getCursorPosition() {
        Vector3 cursorPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
        return cursorPos;
    }

    /**
     * @return the gamecam
     */
    public OrthographicCamera getGameCam() {
        return this.gamecam;
    }

    /**
     * @return the wolrd
     */
    public World getWorld() {
        return this.world;
    }

    /**
     * @return the map
     */
    public TiledMap getMap() {
        return this.map;
    }

    /**
     * Returns player
     * 
     * @return PlayerModel
     */
    public PlayerModel getPlayerModel() {
        return this.player;
    }

    /**
     * Sets the crosshair for the screen
     */
    private void setCrosshairCursor() {
        Gdx.graphics.setSystemCursor(SystemCursor.Crosshair);
    }

    /**
     * @return TextureAtlas atlas
     */
    public TextureAtlas getAtlas() {
        return atlas;
    }

    /**
     * @return an array of the enemies
     */
    public Array<AbstractEnemy> getEnemies() {
        return enemies;
    }

    /**
     * @return FireballManager
     */
    public FireballManager getFireballManager() {
        return fireballManager;
    }

    /**
     * Checks if the players health is 0 or less and transitions to the
     * GameOverScreen
     * Uses Gdx.app.postRunnable to change the screen to ensure thread safety
     * Is called upon in update method
     * 
     * @param dt Time span between the current and last frame. From update method
     */
    private void checksGameOverCondition(float dt) {
        if (player.getHealth() <= 0 && !gameOver) {
            gameOver = true;
            Gdx.app.postRunnable(new Runnable() {

                @Override
                public void run() {
                    game.setScreen(new GameOverScreen(game));
                }

            });
        }
    }

    /**
     * Checks if the player has reached a new wave and transitions to the
     * UpgradeMenuScreen
     * 
     * @param dt
     */
    private void avalibleUpgrades(float dt) {
        int newWave = enemyManager.getWaves();

        if (newWave == 6 && !continueGame) {
            continueGame = true; // Check if the player has reached wave 6 or beyond
            game.setScreen(new VictoryScreen(game, this));
        } else if (newWave != currentWave) {
            currentWave = newWave;
            hud.updateWave(currentWave);
            game.setScreen(new UpgradeMenuScreen(game, player, this));
        }
    }

    /**
     * Resizes the game
     */
    @Override
    public void resize(int width, int height) {
        gamePort.update(width, height);
    }

    @Override
    public void dispose() {
        map.dispose();
        renderer.dispose();
        world.dispose();
        b2dr.dispose();
        hud.dispose();
        atlas.dispose();
        gamecam = null;
        gamePort = null;
        shapeRenderer.dispose();
    }
}