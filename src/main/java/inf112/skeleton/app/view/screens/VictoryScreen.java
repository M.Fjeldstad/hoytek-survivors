package inf112.skeleton.app.view.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import inf112.skeleton.app.GameCreate;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class VictoryScreen extends AbstractScreen {

    // Texture for the victory image
    private final Texture victoryTexture;

    // Image for the victory image
    private Image victoryImage;

    /**
     * Constructor for the VictoryScreen
     * 
     * @param game
     * @param playScreen
     */
    public VictoryScreen(GameCreate game, PlayScreen playScreen) {
        super(game, playScreen);

        victoryTexture = new Texture(Gdx.files.internal("bkgImage/victorybackground.png"));

        createLayout();
    }

    /**
     * Creates the layout of the VictoryScreen
     */
    protected void createLayout() {
        // Create a table to hold the buttons
        Table table = new Table();
        table.setFillParent(true); // Make the table fill the stage

        // Setting the victory image to the screen
        victoryImage = new Image(victoryTexture);
        victoryImage.setFillParent(true);

        
        // Add buttons
        TextButton mainMenuButton = new TextButton("Main Menu", skin);
        mainMenuButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new MainMenuScreen(game));
            }
        });

        TextButton continueButton = new TextButton("Continue", skin);
        continueButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(playScreen);
            }
        });

        // Add buttons to table
        table.add(mainMenuButton).padBottom(-550).expandX().spaceRight(250);
        table.add(continueButton).padBottom(-550).expandX().spaceLeft(250);

        // Add the victory image and the table to the stage
        stage.addActor(victoryImage);
        stage.addActor(table);
    }
}
