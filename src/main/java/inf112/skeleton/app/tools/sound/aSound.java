package inf112.skeleton.app.tools.sound;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.Gdx;

public class aSound {
    private final Music sound;
    private final String fileString;

    /**
     * Constructor for aSound
     * 
     * @param fileString takes in a string
     */
    public aSound(String fileString) {
        this.fileString = fileString;
        sound = Gdx.audio.newMusic(Gdx.files.internal(fileString));
    }

    /**
     * 
     * @return sound
     */
    public Music getSound() {
        return sound;
    }

    /**
     * Plays the sound.
     */
    public void play() {
        sound.play();
    }

    /**
     * Stops the sound.
     */
    public void stop() {
        sound.stop();
    }

    /**
     * Disposes the sound.
     */
    public void dispose() {
        sound.dispose(); 
    }


    /**
     * @return path string for music-file.
     */
    public String getSoundString() {
        return fileString;
    }
}
