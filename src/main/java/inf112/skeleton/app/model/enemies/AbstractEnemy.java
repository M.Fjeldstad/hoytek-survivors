package inf112.skeleton.app.model.enemies;

import java.util.Random;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.view.screens.PlayScreen;

import com.badlogic.gdx.physics.box2d.World;

public abstract class AbstractEnemy {

    protected PlayScreen screen;
    private final World world;
    private Body b2body;
    private Vector2 velocity;
    private int health;
    private int attackDamage;
    private float movementSpeed;
    private EnemyEnum type;
    private Random random = new Random();

    /**
     * Constructor for AbstractEnemy
     * 
     * @param screen
     * @param startingX
     * @param startingY
     * @param health
     * @param movementSpeed
     * @param attackDamage
     */
    public AbstractEnemy(PlayScreen screen, EnemyEnum type, float startingX, float startingY, int health,
            float movementSpeed, int attackDamage) {
        this.screen = screen;
        this.world = screen.getWorld();
        defineEnemy(startingX, startingY);
        this.health = health;
        this.movementSpeed = movementSpeed;
        this.attackDamage = attackDamage;
        this.type = type;
        velocity = new Vector2(-(this.movementSpeed), -(this.movementSpeed) * 2);
    }

    /**
     * Defines the box 2d body of the
     */
    private void defineEnemy(float startingX, float startingY) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(startingX / GameCreate.PPM, startingY / GameCreate.PPM);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        b2body = screen.getWorld().createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(5 / GameCreate.PPM);
        fixtureDef.shape = shape;

        fixtureDef.filter.categoryBits = GameCreate.CATEGORY_ENEMY;
        fixtureDef.filter.maskBits = GameCreate.CATEGORY_WALLS | GameCreate.CATEGORY_FIREBALL
                | GameCreate.CATEGORY_PLAYER | GameCreate.CATEGORY_ENEMY;

        b2body.createFixture(fixtureDef);
        b2body.createFixture(fixtureDef).setUserData(GameCreate.CATEGORY_ENEMY);
    }

    /**
     * Updates the position of the B2BODY
     * 
     * @param dt
     */
    public void update(float dt) {
        Vector2 playerPosition = screen.getPlayerModel().getB2body().getPosition();

        // Calculate the direction from enemy to player
        Vector2 direction = playerPosition.sub(getBody().getPosition()).nor();

        // Add a larger random offset to the direction
        direction.x += (random.nextFloat() - 0.5f) * 2.5f; // 10 times as random
        direction.y += (random.nextFloat() - 0.5f) * 2.5f; // 10 times as random
        direction.nor(); // Normalize the direction after adding the offset

        // Set the enemy's movement speed
        float speed = getMovementSpeed(); // You should define this properly

        // Apply linear velocity to move towards the player
        getBody().setLinearVelocity(direction.scl(speed));
    };

    /**
     * Returns the AbstractEnemy's health
     * 
     * @return the enemies health
     */
    public int getHealth() {
        return this.health;
    }

    /**
     * Returns the b2body of the AbstractEnemy
     * 
     * @return
     */
    public Body getBody() {
        return this.b2body;
    }

    /**
     * Returns the movement speed of the AbstractEnemy
     * 
     * @return movementSpeed
     */
    public float getMovementSpeed() {
        return this.movementSpeed;
    }

    /**
     * Sets the health of an AbstractEnemy
     * 
     * @param deltaHealt
     */
    public void addHealth(int deltaHealt) {
        this.health += deltaHealt;
    }

    /**
     * Removes the b2body from the screens world
     */
    public void dispose() {
        world.destroyBody(b2body);
    }

    /**
     * Returns attackDamage
     * 
     * @return attackDamage
     */
    public int getAttackDamage() {
        return this.attackDamage;
    }

    /**
     * @return position
     */
    public Vector2 getPosition() {
        return b2body.getPosition();
    }

    /**
     * @return type
     */
    public EnemyEnum getType() {
        return type;
    }
}
