package inf112.skeleton.app.model.enemies;

import java.util.Random;

import com.badlogic.gdx.utils.Array;

import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.model.powerups.AbstractPowerUp;
import inf112.skeleton.app.tools.listeners.EnemyCollisionHandler;
import inf112.skeleton.app.tools.sound.SoundManager;
import inf112.skeleton.app.view.screens.PlayScreen;

/**
 *  Manager for all enemies in the game. Handles the creation,
 *  updating and collision of powerups. 
 */
public class EnemyManager {
    private final Array<AbstractEnemy> enemies;

    private final PlayScreen screen;

    private final Array<AbstractEnemy> enemiesToRemove;

    private final EnemyCollisionHandler enemyCollisionHandler;
    private final AbstractEnemyFactory enemyFactory;
    private final SoundManager soundManager;

    private int waveSize = 5;
    private int enemiesKilled;
    private int wave;

    /**
     * Constructor for EnemyManager
     * 
     * @param screen takes in a PlayScreen
     */
    public EnemyManager(PlayScreen screen) {
        this.enemies = new Array<>();
        this.enemiesToRemove = new Array<>();
        this.enemyFactory = new AbstractEnemyFactory(screen);
        this.soundManager = new SoundManager();

        this.screen = screen;
        this.enemiesKilled = 0;
        this.wave = 0;

        enemyCollisionHandler = new EnemyCollisionHandler();
    }

    /**
     * Updates all AbstractEnemies
     * 
     * @param dt
     */
    public void update(float dt) {
        handleCollision();
        spawnEnemies();

        if (enemiesKilled == waveSize) {
            spawnEnemies();
        }
    }

    /**
     * Deals damage to enemies and removes dead AbstractEnemies
     */
    private void handleCollision() {
        for (AbstractEnemy enemy : enemies) {
            if (enemyCollisionHandler.getBodiesToRemove().contains(enemy.getBody(), true)) {
                if (enemy.getHealth() > 0) {
                    enemy.addHealth(-screen.getPlayerModel().getAttackDamage());
                    soundManager.enemy_hit.play();
                } else {
                    enemies.removeValue(enemy, false);
                    enemiesToRemove.add(enemy);
                    enemy.dispose();
                    soundManager.death.play();
                    enemiesKilled++;
                }
            }
        }
        enemyCollisionHandler.clearBodiesToRemove();
    }

    /**
     * Spawns enemies if there are less than 5 enemies
     */
    protected void spawnEnemies() {
        if (enemies.size == 0) {
            for (int i = 0; i < waveSize; i++) {
                AbstractEnemy enemy = createRandomEnemy();
                enemies.add(enemy);
            }

            waveSize += 5;
            wave++;
        }
    }

    /**
     * Creates a random enemy
     * 
     * @return AbstractEnemy
     */
    private AbstractEnemy createRandomEnemy() {
        Random rand = new Random();
        int i = rand.nextInt(2);
        switch (i) {
            case 0:
                AbstractEnemy redEnemy = enemyFactory.spawnRedEnemy(this.getWaves());
                return redEnemy;
            case 1:
            AbstractEnemy blueEnemy = enemyFactory.spawnBlueEnemy(this.getWaves());
            return blueEnemy;
            default:
                throw new IllegalArgumentException("Invalid powerup");
        }
    }

    /**
     * 
     * @return the enemyCollisionHandler
     */
    public EnemyCollisionHandler getEnemyCollisionHandler() {
        return enemyCollisionHandler;
    }

    /**
     * Returns a list of AbstractEnemies that are dead
     * 
     * @return Array<Body>
     */
    public Array<AbstractEnemy> getEnemiesToRemove() {
        return enemiesToRemove;
    }

    /**
     * Returns a list of all AbstractEnemies
     * 
     * @return Array<Body>
     */
    public Array<AbstractEnemy> getEnemies() {
        return enemies;
    }

    /**
     * 
     * @return Returns the number of waves
     */
    public int getWaves() {
        return wave;
    }
}
