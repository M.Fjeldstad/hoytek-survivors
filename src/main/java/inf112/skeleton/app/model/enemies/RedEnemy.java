package inf112.skeleton.app.model.enemies;

import com.badlogic.gdx.physics.box2d.Body;

import inf112.skeleton.app.view.screens.PlayScreen;

public class RedEnemy extends AbstractEnemy {

    public Body b2body;

    /**
     * Constructor for the RedEnemy
     * 
     * @param screen
     * @param x
     * @param y
     * @param health
     * @param movementSpeed
     * @param damage
     */
    public RedEnemy(PlayScreen screen, float xPos, float yPos, int health, float movementSpeed, int damage) {
        super(screen,EnemyEnum.RED_ENEMY, xPos, yPos, health, movementSpeed, damage);
    }

}
