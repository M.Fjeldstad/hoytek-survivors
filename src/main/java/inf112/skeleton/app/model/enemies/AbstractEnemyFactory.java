package inf112.skeleton.app.model.enemies;

import java.util.Random;

import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.view.screens.PlayScreen;

public class AbstractEnemyFactory {

    private final PlayScreen screen;
    private final Random random;

    /**
     * Constructor for AbstractEnemyFactory
     * 
     * @param screen
     */
    public AbstractEnemyFactory(PlayScreen screen) {
        this.screen = screen;
        this.random = new Random();
    }

    /**
     * 
     * @param wave calculates the health of the enemy based of the wave
     * @return a new RedEnemy
     */
    public AbstractEnemy spawnRedEnemy(int wave) {
        int baseHealth = 15;
        int baseDamage = 10;
        int scaledHealth = baseHealth * 1 + (wave / 10); // Scale health based on wave number
        int scaledDamage = baseDamage * 1 + (wave / 10); // Scale damage based on wave number
        return new RedEnemy(screen, random.nextInt(GameCreate.OFFSET, GameCreate.MAP_WIDTH),
                random.nextInt(GameCreate.OFFSET, GameCreate.MAP_HEIGHT), scaledHealth, 1f, scaledDamage);
    }

    /**
     * 
     * @param wave calculates the health of the enemy based of eave
     * @return a new BlueEnemy
     */
    public AbstractEnemy spawnBlueEnemy(int wave) {
        int baseHealth = 36;
        int baseDamage = 15;
        int scaledHealth = baseHealth * 1 + (wave / 10);
        int scaledDamage = baseDamage * 1 + (wave / 10);
        return new BlueEnemy(screen, random.nextInt(GameCreate.OFFSET, GameCreate.MAP_WIDTH),
                random.nextInt(GameCreate.OFFSET, GameCreate.MAP_HEIGHT), scaledHealth, 0.7f, scaledDamage);
    }
}
