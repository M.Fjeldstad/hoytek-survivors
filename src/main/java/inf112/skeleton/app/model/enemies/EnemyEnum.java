package inf112.skeleton.app.model.enemies;

/**
 * Enum for the different types of enemies
 */
public enum EnemyEnum {
    BLUE_ENEMY,
    RED_ENEMY,
}
