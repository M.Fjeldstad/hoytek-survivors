package inf112.skeleton.app.model.player;

/**
 * Enum for the different player states
 */
public enum PlayerEnum {
    STANDING, 
    LEFT, 
    RIGHT, 
    UP, 
    DOWN
}
