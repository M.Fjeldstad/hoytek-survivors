package inf112.skeleton.app.model.player;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.*;
import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.tools.listeners.PlayerModelCollisionHandler;
import inf112.skeleton.app.view.screens.PlayScreen;

import com.badlogic.gdx.Gdx;

/**
 * PlayerModel class
 * Contains logic for the b2body of the player
 * and the player's health, speed, attack damage, and health regen
 */
public class PlayerModel extends Sprite {

    private PlayerEnum currentState;
    private PlayerEnum previousState;
    private Body b2body;
    private World world;
    private PlayScreen screen;

    private int health = 100;
    private int maxHealth = 100;

    private int attackDamage = 12;
    private float movementSpeed = 4f;

    private float maxSpeed = 2f;

    private float timeAccumulator = 0; // Time accumulator for health regen
    private int healthRegen = 1; // Health regeneration rate per second

    private PlayerModelCollisionHandler playerModelCollisionHandler;

    private float damageCooldownTimer = 0; // Cooldown timer for taking damage
    private boolean canTakeDamage = true; // Flag to track if the player can take damage

    /**
     * Constructor for PlayerModel
     * 
     * @param screen the screen the player is on
     */
    public PlayerModel(PlayScreen screen) {
        this.world = screen.getWorld();
        currentState = PlayerEnum.STANDING;
        previousState = PlayerEnum.STANDING;

        this.screen = screen;
        playerModelCollisionHandler = new PlayerModelCollisionHandler();

        definePlayer();
    }

    /**
     * 
     * @return the current state of the player
     */
    public PlayerEnum getState() {
        float xVelocity = b2body.getLinearVelocity().x;
        float yVelocity = b2body.getLinearVelocity().y;
        float velocityThreshold = 0.1f; // Define a suitable threshold for your game

        boolean isMovingHorizontally = Math.abs(xVelocity) > Math.abs(yVelocity);
        boolean isMoving = Math.abs(xVelocity) > velocityThreshold || Math.abs(yVelocity) > velocityThreshold;

        if (!isMoving) {
            return PlayerEnum.STANDING;
        }

        if (isMovingHorizontally) {
            if (xVelocity > 0) {
                return PlayerEnum.RIGHT;
            } else if (xVelocity < 0) {
                return PlayerEnum.LEFT;
            }
        } else {
            if (yVelocity > 0) {
                return PlayerEnum.UP;
            } else if (yVelocity < 0) {
                return PlayerEnum.DOWN;
            }
        }

        return PlayerEnum.STANDING;
    }

    /**
     * Defines the player's body and fixture.
     */
    protected void definePlayer() {
        BodyDef bdef = new BodyDef();
        bdef.position.set(32 / GameCreate.PPM, 32 / GameCreate.PPM);
        bdef.type = BodyDef.BodyType.DynamicBody;
        b2body = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(11 / GameCreate.PPM);

        fdef.shape = shape;
        fdef.filter.categoryBits = GameCreate.CATEGORY_PLAYER;
        fdef.filter.maskBits = GameCreate.CATEGORY_WALLS | GameCreate.CATEGORY_ENEMY | GameCreate.CATEGORY_POWERUP;

        b2body.createFixture(fdef).setUserData(GameCreate.CATEGORY_PLAYER);
    }

    /**
     * 
     * @return the player's health
     */
    public int getHealth() {
        return this.health;
    }

    /**
     * 
     * @param deltaHealth the change in health
     */
    public void addHealth(int deltaHealth) {
        this.health += deltaHealth;

    }

    /**
     * 
     * @return the player's speed
     */
    public float getSpeed() {
        return this.movementSpeed;
    }

    /**
     * @param deltaSpeed the change in speed
     */
    public void addSpeed(float deltaSpeed) {
        this.movementSpeed += deltaSpeed;
    }

    /**
     * 
     * @return the player's max health
     */
    public int getMaxHealth() {
        return this.maxHealth;
    }

    /**
     * 
     * @return the player's max speed
     */
    public float getMaxSpeed() {
        return this.maxSpeed;
    }

    /**
     * 
     * @param deltaMaxSpeed the amount the maxSpeed should be changed by
     */
    public void addMaxSpeed(float deltaMaxSpeed) {
        this.maxSpeed += deltaMaxSpeed;
    }

    /**
     * Updates the maxHealth of the player by a given value
     * 
     * @param deltaMaxHealth the amount the maxHealth should be changed by
     */
    public void addMaxHealth(int deltaMaxHealth) {
        this.maxHealth += deltaMaxHealth;
    }

    /**
     * @return attackDamage
     */
    public int getAttackDamage() {
        return this.attackDamage;
    }

    /**
     * 
     * @param deltaAttackDamage the amount the attackDamage should be changed by
     */
    public void addAttackDamage(int deltaAttackDamage) {
        this.attackDamage += deltaAttackDamage;
    }

    /**
     * 
     * @return the player's body
     */
    public Body getB2body() {
        return b2body;
    }

    /**
     * Handles the collision between the player and the enemies
     * If the player is hit, the player's health is reduced by the enemy's attack
     * damage
     */
    public void handleCollision() {
        if (canTakeDamage) {
            int attackDamage = 0;

            if (screen.getEnemies().notEmpty()) {
                attackDamage = screen.getEnemies().first().getAttackDamage();
            }

            if (playerModelCollisionHandler.getIsHit()) {
                addHealth(-attackDamage);
                canTakeDamage = false; // Set flag to false to start cooldown
                damageCooldownTimer = 0.5f; // Set cooldown timer to 1/2 second
                playerModelCollisionHandler.setIsHitFalse();
            }
        } else {
            // Update cooldown timer
            damageCooldownTimer -= Gdx.graphics.getDeltaTime();
            if (damageCooldownTimer <= 0) {
                canTakeDamage = true; // Reset flag when cooldown ends
                damageCooldownTimer = 0; // Reset cooldown timer
            }
        }
    }

    /**
     * Updates the player
     * Calls the handle collision method
     * and regens the player's health
     * 
     * @param dt
     */
    public void update(float dt) {
        timeAccumulator += dt;
        if (timeAccumulator >= 1.0) {
            if (health < maxHealth) {
                health += healthRegen; // Increment health by the regen rate
            }
            timeAccumulator -= 1.0; // Reset the accumulator
        }

        handleCollision();

    }

    /**
     * 
     * @return the player's health regen rate
     */
    public int getHealthRegen() {
        return healthRegen;
    }

    /**
     * 
     * @param healthRegen the new health regen rate
     */
    public void setHealthRegen(int healthRegen) {
        this.healthRegen = healthRegen;
    }

    /**
     * 
     * @return the player's collision handler
     */
    public PlayerModelCollisionHandler getPlayerModelCollisionHandler() {
        return playerModelCollisionHandler;
    }
}
