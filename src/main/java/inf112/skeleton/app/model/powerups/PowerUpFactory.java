package inf112.skeleton.app.model.powerups;

import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.view.screens.PlayScreen;


public class PowerUpFactory {

    private PlayScreen screen;

    /**
     * Constructor for PowerUpFactory
     * @param screen takes in a playscreen
     */
    public PowerUpFactory(PlayScreen screen) {
        this.screen = screen;
    }

    /**
     * Method for creating a speed powerup
     * @param playerModel playermodel
     * @param xPos x position
     * @param yPos y position
     * @return speed powerup
     */
    public AbstractPowerUp speedPowerUp(PlayerModel playerModel, int xPos, int yPos) {
        return new SpeedPowerUp(screen, playerModel, xPos, yPos);
    }

     /**
     * Method for creating a damage powerup
     * @param playerModel playermodel
     * @param xPos x position
     * @param yPos y position
     * @return damage powerup
     */
    public AbstractPowerUp damagePowerUp(PlayerModel playerModel, int xPos, int yPos) {
        return new DamagePowerUp(screen, playerModel, xPos, yPos);
    }
}