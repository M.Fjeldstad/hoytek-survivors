package inf112.skeleton.app.model.powerups;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.view.screens.PlayScreen;

public abstract class AbstractPowerUp {

    protected PlayScreen screen;
    protected PlayerModel playerModel;
    protected Vector2 position;
    private World world;
    private Body b2body;
    private int xPos;
    private int yPos;
    protected boolean isActive = false;
    protected boolean isRemovable = false;
    private float powerUpDuration = 10;
    private PowerUpEnum type;

    public AbstractPowerUp(PlayScreen screen, PlayerModel playerModel, PowerUpEnum type, int xPos, int yPos) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.position = new Vector2(xPos, yPos);
        this.type = type;
        this.world = screen.getWorld();
        this.screen = screen;
        this.playerModel = playerModel;
        definePowerUp();
    }

    /**
     * Method to remove the effect of the power up
     */
    protected abstract void removePowerUpEffect();

    /**
     * Method to apply the effect of the power up
     */
    protected abstract void applyPowerUpEffect();

    /**
     * Method to remove the power up
     */
    protected void removePowerUp() {
        removePowerUpEffect();
    }

    /**
     * Method to apply the power up
     */
    protected void applyPowerUp() {
        applyPowerUpEffect();
        isActive = true;
        dispose();
    }


    /**
     * Method to define the power up b2body
     */
    private void definePowerUp() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(xPos / GameCreate.PPM, yPos / GameCreate.PPM);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        b2body = screen.getWorld().createBody(bodyDef);

        FixtureDef fixtureDef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6 / GameCreate.PPM);
        fixtureDef.shape = shape;

        fixtureDef.filter.categoryBits = GameCreate.CATEGORY_POWERUP;
        fixtureDef.filter.maskBits = GameCreate.CATEGORY_PLAYER;
        fixtureDef.isSensor = true;
        b2body.createFixture(fixtureDef);
        b2body.createFixture(fixtureDef).setUserData(GameCreate.CATEGORY_POWERUP);
    }

    /**
     * Method to update the power up effects
     * @param dt the time since last frame
     */
    public void update(float dt) {
        if (isActive) {
            powerUpDuration -= dt;
            if (powerUpDuration <= 0) {
                removePowerUp();
                isRemovable = true;
            }
        }
    }

    /**
     * Method to dispose the power up
     */
    public void dispose() {
        world.destroyBody(b2body);
    }

    /**
     * 
     * @return the type of the power up
     */
    public PowerUpEnum getType() {
        return type;
    }

    /**
     * 
     * @return the position of the power up
     */
    public Vector2 getPosition() {
        return position;
    }

    /**
     * 
     * @return the duration of the power up
     */
    public float getPowerUpDuration() {
        return powerUpDuration;
    }

    /**
     * 
     * @param powerUpDuration the duration of the power up
     */
    public void setPowerUpDuration(float powerUpDuration) {
        this.powerUpDuration = powerUpDuration;
    }

    /**
     * 
     * @return the body of the power up
     */
    public Body getBody() {
        return b2body;
    }
}
