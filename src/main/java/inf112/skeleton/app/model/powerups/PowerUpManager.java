package inf112.skeleton.app.model.powerups;

import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.tools.listeners.PowerUpCollisionHandler;
import inf112.skeleton.app.view.screens.PlayScreen;

import java.util.Iterator;

import java.util.ArrayList;
import java.util.Random;

/**
 * Manager for all powerups in the game. Handles the creation,
 * updating and collision of powerups. Also keeps track of all
 * active powerups.
 * 
 
 */
public class PowerUpManager {

    private PlayerModel playerModel;
    private final PowerUpFactory powerUpFactory;
    private final ArrayList<AbstractPowerUp> powerUps;
    private final ArrayList<AbstractPowerUp> powerUpsToRemove;
    private final ArrayList<AbstractPowerUp> activePowerUps;
    private final PowerUpCollisionHandler powerUpCollisionHandler;
    private float timeSinceLastPowerUp;
    private static final float SPAWN_INTERVAL = 5;

    public PowerUpManager(PlayScreen screen, PlayerModel playerModel) {
        this.powerUps = new ArrayList<AbstractPowerUp>();
        this.activePowerUps = new ArrayList<AbstractPowerUp>();
        this.playerModel = playerModel;
        this.powerUpFactory = new PowerUpFactory(screen);
        this.powerUpCollisionHandler = new PowerUpCollisionHandler();
        this.powerUpsToRemove = new ArrayList<AbstractPowerUp>();
    }

    /**
     * Updates all powerups
     * 
     * @param dt time since last frame
     */
    public void update(float dt) {
        timeSinceLastPowerUp += dt;

        if (timeSinceLastPowerUp >= SPAWN_INTERVAL) {
            createRandomPowerUp();
            timeSinceLastPowerUp = 0;
        }

        // Count down the duration of the active power-ups. No need to update the power
        // ups that hasn't been touched
        Iterator<AbstractPowerUp> iterator = activePowerUps.iterator();
        while (iterator.hasNext()) {
            AbstractPowerUp powerUp = iterator.next();
            powerUp.update(dt);
            if (powerUp.isRemovable) {
                iterator.remove();
            }
        }
        handleCollision();
    }

    /**
     * Handles collision between player and powerups
     * Applies the powerup if there is a collision
     */
    private void handleCollision() {
        Iterator<AbstractPowerUp> powerUpIterator = powerUps.iterator();
        while (powerUpIterator.hasNext()) {
            AbstractPowerUp powerUp = powerUpIterator.next();
            if (powerUpCollisionHandler.getBodiesToRemove().contains(powerUp.getBody(), true)) {
                powerUp.applyPowerUp();
                powerUpsToRemove.add(powerUp);
                activePowerUps.add(powerUp);
                powerUpIterator.remove(); // Safely remove the powerUp from the list;
            }
        }
        powerUpCollisionHandler.getBodiesToRemove().clear();
    }

    /**
     * Creates a random powerup
     * 
     * @return the powerup
     */
    private AbstractPowerUp createRandomPowerUp() {
        Random rand = new Random();
        int xPos = rand.nextInt(GameCreate.OFFSET, GameCreate.MAP_HEIGHT);
        int yPos = rand.nextInt(GameCreate.OFFSET, GameCreate.MAP_WIDTH);
        int i = rand.nextInt(2);
        switch (i) {
            case 0:
                AbstractPowerUp speedPowerUp = powerUpFactory.speedPowerUp(playerModel, xPos, yPos);
                powerUps.add(speedPowerUp);
                return speedPowerUp;
            case 1:
                AbstractPowerUp damagPowerUp = powerUpFactory.damagePowerUp(playerModel, xPos, yPos);
                powerUps.add(damagPowerUp);
                return damagPowerUp;
            default:
                throw new IllegalArgumentException("Invalid powerup");
        }
    }

    /**
     * 
     * @return the powerup collision handler
     */
    public PowerUpCollisionHandler getPowerUpCollisionHandler() {
        return powerUpCollisionHandler;
    }

    /**
     * 
     * @return an array of all powerups
     */

    public ArrayList<AbstractPowerUp> getPowerUps() {
        return powerUps;
    }

    /**
     * 
     * @return an array of all active powerups
     */

    public ArrayList<AbstractPowerUp> getActivePowerUps() {
        return activePowerUps;
    }

    /**
     * 
     * @return an array of all powerups thar shuld be removed
     */
    public ArrayList<AbstractPowerUp> getPowerUpsToRemove() {
        return powerUpsToRemove;
    }

}
