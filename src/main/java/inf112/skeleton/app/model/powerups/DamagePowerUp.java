package inf112.skeleton.app.model.powerups;

import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.view.screens.PlayScreen;

public class DamagePowerUp extends AbstractPowerUp {

    private PlayerModel playerModel;

    /**
     * Constructor for DamagePowerUp
     * @param screen playscreen
     * @param playerModel playermodel
     * @param xPos spawn x position
     * @param yPos spawn y position
     */
    public DamagePowerUp(PlayScreen screen, PlayerModel playerModel, int xPos, int yPos) {
        super(screen, playerModel, PowerUpEnum.DAMAGE_BOOST, xPos, yPos);
        this.playerModel = playerModel;
    }
    
    protected void removePowerUpEffect() {
        playerModel.addAttackDamage(-5); ;
    }

    protected void applyPowerUpEffect() {
        playerModel.addAttackDamage(+5);
    }

}   
