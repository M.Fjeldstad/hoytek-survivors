package inf112.skeleton.app.model.powerups;

import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.view.screens.PlayScreen;

public class SpeedPowerUp extends AbstractPowerUp {

    private PlayerModel playerModel;

    public SpeedPowerUp(PlayScreen screen, PlayerModel playerModel, int xPos, int yPos) {
        super(screen, playerModel, PowerUpEnum.SPEED_BOOST, xPos, yPos);
        this.playerModel = playerModel;
    }

    protected void removePowerUpEffect() {
        playerModel.addMaxSpeed(-.5f);
        playerModel.addSpeed(-.5f);
    }

    protected void applyPowerUpEffect() {
        playerModel.addMaxSpeed(+.5f);
        playerModel.addSpeed(+.5f);
    }
}
