package inf112.skeleton.app.model.powerups;

/**
 * Enum for the different powerups
 */
public enum PowerUpEnum {
    SPEED_BOOST,
    DAMAGE_BOOST,
}
