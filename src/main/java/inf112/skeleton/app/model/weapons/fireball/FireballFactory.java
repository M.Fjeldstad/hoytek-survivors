package inf112.skeleton.app.model.weapons.fireball;

import inf112.skeleton.app.view.screens.PlayScreen;


public class FireballFactory {
    
        private final PlayScreen screen;
    
        public FireballFactory(PlayScreen screen) {
            this.screen = screen;
        }
    
        public Fireball createFireball() {
            return new Fireball(screen);
        }
}
