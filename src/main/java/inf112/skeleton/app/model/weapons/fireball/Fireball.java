package inf112.skeleton.app.model.weapons.fireball;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.view.screens.PlayScreen;

import com.badlogic.gdx.physics.box2d.Body;

public class Fireball {

    private final World world;
    private Body b2body;
    private float x;
    private float y;

    /**
     * Constructor for Fireball class.
     *
     * @param screen The play screen.
     */
    public Fireball(PlayScreen screen) {
        this.world = screen.getWorld();
        this.x = screen.getPlayerModel().getB2body().getPosition().x;
        this.y = screen.getPlayerModel().getB2body().getPosition().y;

        defineEntity();
    }

    /**
     * Defines the fireball
     */
    protected void defineEntity() {
        BodyDef bdef = new BodyDef();
        bdef.position.set(x, y);
        bdef.type = BodyDef.BodyType.DynamicBody;
        b2body = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(2 / GameCreate.PPM);

        fdef.shape = shape;
        fdef.filter.categoryBits = GameCreate.CATEGORY_FIREBALL;
        fdef.filter.maskBits = GameCreate.CATEGORY_WALLS | GameCreate.CATEGORY_ENEMY;

        b2body.createFixture(fdef).setUserData(GameCreate.CATEGORY_FIREBALL);
    }

    /**
     * Sets the velocity of the fireball
     * 
     * @param velocity
     */
    public void setLinearVelocity(Vector2 velocity) {
        this.b2body.setLinearVelocity(velocity);
    }

    /**
     * Disposes the fireball
     */
    public void dispose() {
        world.destroyBody(b2body);
    }

    /**
     * @return position
     */
    public Vector2 getPosition() {
        return b2body.getPosition();
    }

    /**
     * @return b2body
     */
    public Body getB2body() {
        return b2body;
    }
}
