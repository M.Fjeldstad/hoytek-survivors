package inf112.skeleton.app.model.weapons.fireball;

import inf112.skeleton.app.tools.listeners.FireballCollisionHandler;
import inf112.skeleton.app.tools.sound.SoundManager;
import inf112.skeleton.app.view.screens.PlayScreen;

import java.util.Iterator;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;

/**
 * Manager for all fireball in the game. Handles the creation,
 * updating and collision of powerups.
 */
public class FireballManager {
    private final Array<Fireball> fireballs;
    private final FireballFactory fireballFactory;
    private final Array<Fireball> fireballsToRemove;
    private final FireballCollisionHandler fireballCollisionHandler;
    private float timeSinceLastPowerUp;
    private float spawnInterval = 2;
    private final PlayScreen screen;
    private final SoundManager soundManager;

    /**
     * Constructor for FireballManager
     * 
     * @param screen takes in a PlayScreen
     */
    public FireballManager(PlayScreen screen) {
        this.fireballs = new Array<>();
        this.fireballsToRemove = new Array<>();
        this.screen = screen;
        this.fireballFactory = new FireballFactory(screen);
        fireballCollisionHandler = new FireballCollisionHandler();
        this.soundManager = new SoundManager();
    }

    /**
     * Updates all fireballs
     * 
     * @param dt time since last frame
     */
    public void update(float dt) {
        timeSinceLastPowerUp += dt;

        if (timeSinceLastPowerUp >= spawnInterval) {
            Vector2 direction = calculateVector();
            createConeFireball(direction);
            timeSinceLastPowerUp = 0;
            soundManager.fireball_shoot.play();
        }

        handleCollision();
    }

    /**
     * Handles collision between enemeis and fireballs
     * 
     */
    private void handleCollision() {
        Array<Body> bodiesToRemove = fireballCollisionHandler.getBodiesToRemove();
        Iterator<Fireball> fireballIterator = fireballs.iterator();
        while (fireballIterator.hasNext()) {
            Fireball fireball = fireballIterator.next();
            if (bodiesToRemove.contains(fireball.getB2body(), true)) {
                fireballsToRemove.add(fireball);
                screen.getWorld().destroyBody(fireball.getB2body());
                fireballIterator.remove();
            }
        }
        bodiesToRemove.clear();
    }

    /**
     * Creates a cone of fireballs
     * 
     * @param direction the direction of the cone
     */
    private void createConeFireball(Vector2 direction) {
        for (int i = 0; i < 3; i++) {
            Fireball coneFireball = fireballFactory.createFireball();
            Vector2 coneVelocity = direction.cpy().rotateDeg(-15 + i * 15); // Adjust angle as needed
            coneFireball.setLinearVelocity(coneVelocity);
            fireballs.add(coneFireball);
        }
    }

    /**
     * Calculates the direction of the fireball
     * 
     * @return the direction of the fireball
     */
    private Vector2 calculateVector() {
        // Player position
        Vector2 playerPosition = new Vector2(screen.getPlayerModel().getB2body().getPosition().x,
        screen.getPlayerModel().getB2body().getPosition().y);

        // Cursor position
        Vector3 cursorPosition = screen.getCursorPosition();

        // Convert cursor position to game world coordinates
        screen.getGameCam().unproject(cursorPosition);

        //
        Vector2 direction = new Vector2(cursorPosition.x, cursorPosition.y).sub(playerPosition).nor();

        return direction;
    }

    /**
     * 
     * @return fireballCollisionHandler
     */
    public FireballCollisionHandler getFireballCollisionHandler() {
        return fireballCollisionHandler;
    }

    /**
     * 
     * @return an array of all fireballs
     */
    public Array<Fireball> getFireballs() {
        return fireballs;
    }

    /**
     * 
     * @return an array of fireballs to remove
     */
    public Array<Fireball> getFireballsToRemove() {
        return fireballsToRemove;
    }

    /**
     * 
     * @return spawnInterval
     */
    public Float getSpawnInterval() {
        return spawnInterval;
    }

    /**
     * Sets the spawn interval
     * 
     * @param spawnInterval
     */
    public void setSpawnInterval(float spawnInterval) {
        this.spawnInterval = spawnInterval;
    }

}
