package inf112.skeleton.app.tools.sound;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.*;

import com.badlogic.gdx.Audio;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;

import com.badlogic.gdx.graphics.GL20;

import inf112.skeleton.app.GameCreate;


public class SoundTest {

    private SoundManager soundManager;

    private GameCreate app;


    @BeforeAll
	static void setUpBeforeAll() {
        Gdx.gl = mock(GL20.class);       
        Gdx.gl20 = mock(GL20.class);
        Gdx.audio = mock(Audio.class);
        Gdx.files = mock(Files.class);
	}   

	@BeforeEach
	void setUpBeforeEach() {
        app = mock(GameCreate.class);
        HeadlessApplicationConfiguration config = new HeadlessApplicationConfiguration();
        new HeadlessApplication(app, config);
        soundManager = new SoundManager();
        
        
	}

    @Test
    public void testMainMenuMusic() {
        soundManager.themeMusic.play();
        assertTrue(soundManager.themeMusic.getSoundString() =="sounds/theme.wav");
        soundManager.themeMusic.stop();
        soundManager.themeMusic.dispose();
    }

    @Test
    public void testHit() {
        soundManager.themeMusic.play();
        assertTrue(soundManager.enemy_hit.getSoundString() =="sounds/enemy_hit.ogg");
        soundManager.themeMusic.stop();
        soundManager.themeMusic.dispose();
    }

    @Test
    public void testFireballShoot() {
        soundManager.themeMusic.play();
        assertTrue(soundManager.fireball_shoot.getSoundString() =="sounds/fireball_shoot.ogg");
        soundManager.themeMusic.stop();
        soundManager.themeMusic.dispose();
    }

    @Test
    public void testDeath() {
        soundManager.themeMusic.play();
        assertTrue(soundManager.death.getSoundString() =="sounds/death.ogg");
        soundManager.themeMusic.stop();
        soundManager.themeMusic.dispose();
    }

    @Test
    public void testGetSound() {
        Music expectedSound = Gdx.audio.newMusic(Gdx.files.internal("sounds/theme.wav"));
        assertNotNull(expectedSound);
    }

    @Test
    public void testInitalization() {
        aSound sound = new aSound("sounds/theme.wav");
        assertNotNull(sound);
    }
}