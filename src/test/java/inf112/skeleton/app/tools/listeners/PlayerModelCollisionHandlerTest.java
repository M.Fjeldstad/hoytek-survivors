package inf112.skeleton.app.tools.listeners;


import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.Array;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import inf112.skeleton.app.GameCreate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class PlayerModelCollisionHandlerTest {

    private PlayerModelCollisionHandler playerModelCollisionHandler;

    @BeforeEach
    public void setUp() {
        playerModelCollisionHandler = new PlayerModelCollisionHandler();
    }

    @Test
    public void handleCollisionTest() {
        // Create mock Contact and Fixture objects
        Contact contact = mock(Contact.class);
        Fixture enemyFixture = mock(Fixture.class);
        Fixture playerFixture = mock(Fixture.class);

        Short categoryPlayer = GameCreate.CATEGORY_PLAYER;
        Short categoryEnemy = GameCreate.CATEGORY_ENEMY;

        when(contact.getFixtureA()).thenReturn(enemyFixture);
        when(contact.getFixtureB()).thenReturn(playerFixture);

        when(enemyFixture.getUserData()).thenReturn(GameCreate.CATEGORY_ENEMY);
        when(playerFixture.getUserData()).thenReturn(GameCreate.CATEGORY_PLAYER);

        playerModelCollisionHandler.handleCollision(contact);
        playerModelCollisionHandler.isCollisionBetween(contact, categoryPlayer, categoryEnemy);
    
        assertTrue(playerModelCollisionHandler.getIsHit());
    }

    @Test
    void getBodiesToRemoveTest() {
        Array<Body> bodiesToRemove = playerModelCollisionHandler.getBodiesToRemove();
        assertNull(bodiesToRemove);
    }

    @Test
    void setIsHitFalseTest() {
        playerModelCollisionHandler.setIsHitFalse();
        assertFalse(playerModelCollisionHandler.getIsHit());
    }

    @Test
    void clearBodiesToRemoveTest() {
        playerModelCollisionHandler.clearBodiesToRemove();
    }

}