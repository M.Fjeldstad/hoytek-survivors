package inf112.skeleton.app.tools.listeners;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.Array;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import inf112.skeleton.app.GameCreate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class PowerUpCollisionHandlerTest {

    private PowerUpCollisionHandler powerUpCollisionHandler;

    @BeforeEach
    public void setUp() {
        powerUpCollisionHandler = new PowerUpCollisionHandler();
    }

    @Test
    public void handleCollisionEnemyFireballCollisionAddsBodyToRemoveList() {
        // Create mock Contact and Fixture objects
        Contact contact = mock(Contact.class);
        Fixture playerFixture = mock(Fixture.class);
        Fixture powerUpFixture = mock(Fixture.class);
        Body powerUpBody = mock(Body.class);

        // Set up the contact to return the mock fixtures
        when(contact.getFixtureA()).thenReturn(playerFixture);
        when(contact.getFixtureB()).thenReturn(powerUpFixture);

        // Set user data for the fixtures
        when(playerFixture.getUserData()).thenReturn(GameCreate.CATEGORY_PLAYER);
        when(powerUpFixture.getUserData()).thenReturn(GameCreate.CATEGORY_POWERUP);

        // Set up the enemy fixture to return the mock body
        when(powerUpFixture.getBody()).thenReturn(powerUpBody);

        // Call the handleCollision method
        powerUpCollisionHandler.handleCollision(contact);

        // Assert that the enemy's body was added to the bodiesToRemove list
        assertTrue(powerUpCollisionHandler.getBodiesToRemove().contains(powerUpBody, true));
    }

    @Test
    public void clearBodiesToRemoveRemovesAllBodies() {
        // Create mock Body objects
        Body body1 = mock(Body.class);
        Body body2 = mock(Body.class);

        // Add the mock bodies to the bodiesToRemove list
        powerUpCollisionHandler.getBodiesToRemove().addAll(body1, body2);

        // Call the clearBodiesToRemove method
        powerUpCollisionHandler.clearBodiesToRemove();

        // Assert that the bodiesToRemove list is empty after clearing
        assertTrue(powerUpCollisionHandler.getBodiesToRemove().isEmpty());
    }

    @Test
    public void getBodiesToRemoveReturnsEmptyArrayInitially() {
        // Call the getBodiesToRemove method and assert that it returns an empty array
        Array <Body> bodiesToRemove = powerUpCollisionHandler.getBodiesToRemove();
        assertTrue(bodiesToRemove.isEmpty());
    }
}