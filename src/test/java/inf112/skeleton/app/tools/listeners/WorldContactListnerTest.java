package inf112.skeleton.app.tools.listeners;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import inf112.skeleton.app.GameCreate;

class WorldContactListnerTest {

    // Mocked CollisionHandler object for testing
    private WorldContactListener worldContactListner;

    private PowerUpCollisionHandler powerUpCollisionHandler;
    private FireballCollisionHandler fireballCollisionHandler;
    private EnemyCollisionHandler enemyCollisionHandler;
    private PlayerModelCollisionHandler playerCollisionHandler;

    @BeforeEach
    void setUp() {
        // Initialize Mockito annotations
        MockitoAnnotations.openMocks(this);
        powerUpCollisionHandler = mock(PowerUpCollisionHandler.class);
        fireballCollisionHandler = mock(FireballCollisionHandler.class);
        enemyCollisionHandler = mock(EnemyCollisionHandler.class);
        playerCollisionHandler = mock(PlayerModelCollisionHandler.class);

        // Create a mocked CollisionHandler object
        worldContactListner = new WorldContactListener(powerUpCollisionHandler, fireballCollisionHandler, enemyCollisionHandler, playerCollisionHandler);
    }

    @Test
    void beginContactTest() {
        // Mock Contact and Fixture objects
        Contact contact = mock(Contact.class);
        Fixture fixtureA = mock(Fixture.class);
        Fixture fixtureB = mock(Fixture.class);

        when(fixtureA.getUserData()).thenReturn(null);
        when(fixtureB.getUserData()).thenReturn(null);

        when(contact.getFixtureA()).thenReturn(fixtureA);
        when(contact.getFixtureB()).thenReturn(fixtureB);


        worldContactListner.beginContact(contact);
    }

    @Test
    void isCollisionBetweenTest() {
        // Mock Contact and Fixture objects
        Contact contact = mock(Contact.class);
        Fixture fixtureA = mock(Fixture.class);
        Fixture fixtureB = mock(Fixture.class);

        // Mocking getUserData() to avoid NullPointerException
        when(fixtureA.getUserData()).thenReturn(null);
        when(fixtureB.getUserData()).thenReturn(null);

        when(contact.getFixtureA()).thenReturn(fixtureA);
        when(contact.getFixtureB()).thenReturn(fixtureB);

        // Call the method being tested
        boolean result = worldContactListner.isCollisionBetween(contact, (short) 1, (short) 2);
        // Check that the method returns false
        assertFalse(result);
    }
}
