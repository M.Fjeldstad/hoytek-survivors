package inf112.skeleton.app.tools.listeners;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.Array;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import inf112.skeleton.app.GameCreate;

public class FireballCollisionHandlerTest {

    private FireballCollisionHandler collisionHandler;
    private Contact contact;
    private Fixture fixtureA;
    private Fixture fixtureB;
    private Body bodyA;
    private Body bodyB;

    @BeforeEach
    public void setUp() {
        collisionHandler = new FireballCollisionHandler();
        contact = mock(Contact.class);
        fixtureA = mock(Fixture.class);
        fixtureB = mock(Fixture.class);
        bodyA = mock(Body.class);
        bodyB = mock(Body.class);

        // Mocking getUserData() to avoid NullPointerException
        when(fixtureA.getUserData()).thenReturn(null);
        when(fixtureB.getUserData()).thenReturn(null);

        when(fixtureA.getBody()).thenReturn(bodyA);
        when(fixtureB.getBody()).thenReturn(bodyB);
        when(contact.getFixtureA()).thenReturn(fixtureA);
        when(contact.getFixtureB()).thenReturn(fixtureB);
    }

    @Test
    public void handleCollision_AddsBodyToRemoveList_WhenCollidingWithEnemies() {
        // Simulate collision between fireball and enemy
        when(fixtureA.getUserData()).thenReturn(GameCreate.CATEGORY_FIREBALL);
        when(fixtureB.getUserData()).thenReturn(GameCreate.CATEGORY_ENEMY);

        // Call the method being tested
        collisionHandler.handleCollision(contact);

        // Check that the body has been added to the list of bodies to remove
        Array<Body> bodiesToRemove = collisionHandler.getBodiesToRemove();
        assertTrue(bodiesToRemove.contains(bodyA, true));
    }

    @Test
    public void handleCollision_DoesNotAddBodyToRemoveList_WhenNotCollidingWithEnemies() {
        // Simulate collision with something other than enemy
        when(fixtureA.getUserData()).thenReturn(GameCreate.CATEGORY_FIREBALL);
        when(fixtureB.getUserData()).thenReturn(GameCreate.CATEGORY_WALLS);

        // Call the method being tested
        collisionHandler.handleCollision(contact);

        // Check that the body has not been added to the list of bodies to remove
        Array<Body> bodiesToRemove = collisionHandler.getBodiesToRemove();
        assertTrue(bodiesToRemove.contains(bodyA, true), "Body should be added to remove list");
        assertFalse(bodiesToRemove.contains(bodyB, false), "Body should not be added to remove list");
    }

    @Test
    public void clearBodiesToRemove_RemovesAllBodiesFromList() {
        // Add some bodies to the list
        collisionHandler.getBodiesToRemove().add(bodyA);
        collisionHandler.getBodiesToRemove().add(bodyB);

        // Call the method being tested
        collisionHandler.clearBodiesToRemove();

        // Check that the list is empty
        Array<Body> bodiesToRemove = collisionHandler.getBodiesToRemove();
        assertTrue(bodiesToRemove.isEmpty());
    }
}