package inf112.skeleton.app.tools.listeners;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.Array;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import inf112.skeleton.app.GameCreate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class EnemyCollisionHandlerTest {

    private EnemyCollisionHandler enemyCollisionHandler;

    @BeforeEach
    public void setUp() {
        enemyCollisionHandler = new EnemyCollisionHandler();
    }

    @Test
    public void handleCollisionEnemyFireballCollisionAddsBodyToRemoveList() {
        // Create mock Contact and Fixture objects
        Contact contact = mock(Contact.class);
        Fixture enemyFixture = mock(Fixture.class);
        Fixture fireballFixture = mock(Fixture.class);
        Body enemyBody = mock(Body.class);

        // Set up the contact to return the mock fixtures
        when(contact.getFixtureA()).thenReturn(enemyFixture);
        when(contact.getFixtureB()).thenReturn(fireballFixture);

        // Set user data for the fixtures
        when(enemyFixture.getUserData()).thenReturn(GameCreate.CATEGORY_ENEMY);
        when(fireballFixture.getUserData()).thenReturn(GameCreate.CATEGORY_FIREBALL);

        // Set up the enemy fixture to return the mock body
        when(enemyFixture.getBody()).thenReturn(enemyBody);

        // Call the handleCollision method
        enemyCollisionHandler.handleCollision(contact);

        // Assert that the enemy's body was added to the bodiesToRemove list
        assertTrue(enemyCollisionHandler.getBodiesToRemove().contains(enemyBody, true));
    }

    @Test
    public void clearBodiesToRemoveRemovesAllBodies() {
        // Create mock Body objects
        Body body1 = mock(Body.class);
        Body body2 = mock(Body.class);

        // Add the mock bodies to the bodiesToRemove list
        enemyCollisionHandler.getBodiesToRemove().addAll(body1, body2);

        // Call the clearBodiesToRemove method
        enemyCollisionHandler.clearBodiesToRemove();

        // Assert that the bodiesToRemove list is empty after clearing
        assertTrue(enemyCollisionHandler.getBodiesToRemove().isEmpty());
    }

    @Test
    public void getBodiesToRemoveReturnsEmptyArrayInitially() {
        // Call the getBodiesToRemove method and assert that it returns an empty array
        Array <Body> bodiesToRemove = enemyCollisionHandler.getBodiesToRemove();
        assertTrue(bodiesToRemove.isEmpty());
    }
}