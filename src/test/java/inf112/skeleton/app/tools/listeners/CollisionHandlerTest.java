package inf112.skeleton.app.tools.listeners;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;

class CollisionHandlerTest {

    // Mocked CollisionHandler object for testing
    private CollisionHandler collisionHandler;

    @BeforeEach
    void setUp() {
        // Initialize Mockito annotations
        MockitoAnnotations.openMocks(this);

        // Create a mocked CollisionHandler object
        collisionHandler = spy(CollisionHandler.class);
    }

    @Test
    void isCollisionBetweenValidCollisionReturnsTrue() {
        // Mock Contact and Fixture objects
        Contact contact = mock(Contact.class);
        Fixture fixtureA = mock(Fixture.class);
        Fixture fixtureB = mock(Fixture.class);

        // Set user data for the fixtures
        when(fixtureA.getUserData()).thenReturn((short) 1);
        when(fixtureB.getUserData()).thenReturn((short) 2);

        // Set up the contact to return the mocked fixtures
        when(contact.getFixtureA()).thenReturn(fixtureA);
        when(contact.getFixtureB()).thenReturn(fixtureB);

        // Call the isCollisionBetween method and assert the result
        assertTrue(collisionHandler.isCollisionBetween(contact, (short) 1, (short) 2));
    }

    @Test
    void getFixtureByCategoryValidCategoryReturnsFixture() {
        // Mock Contact and Fixture objects
        Contact contact = mock(Contact.class);
        Fixture fixtureA = mock(Fixture.class);
        Fixture fixtureB = mock(Fixture.class);

        // Set user data for the fixtures
        when(fixtureA.getUserData()).thenReturn((short) 1);
        when(fixtureB.getUserData()).thenReturn((short) 2);

        // Set up the contact to return the mocked fixtures
        when(contact.getFixtureA()).thenReturn(fixtureA);
        when(contact.getFixtureB()).thenReturn(fixtureB);

        // Call the getFixtureByCategory method and assert the result
        assertEquals(fixtureA, collisionHandler.getFixtureByCategory(contact, (short) 1));
    }

    @Test
    void beginContactValidContactCallsHandleCollision() {
        // Mock Contact object
        Contact contact = mock(Contact.class);

        // Call the beginContact method
        collisionHandler.beginContact(contact);

        // Verify that the handleCollision method was called
        verify(collisionHandler).handleCollision(contact);
    }
}
