package inf112.skeleton.app.view.enemyView;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

import inf112.skeleton.app.model.enemies.AbstractEnemy;
import inf112.skeleton.app.model.enemies.BlueEnemy;
import inf112.skeleton.app.model.enemies.RedEnemy;
import inf112.skeleton.app.view.enemyView.AbstractEnemyView;
import inf112.skeleton.app.view.enemyView.BlueEnemyView;
import inf112.skeleton.app.view.enemyView.RedEnemyView;
import inf112.skeleton.app.view.screens.PlayScreen;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.lang.reflect.Field;

class EnemyViewTest {
    private PlayScreen mockScreen;
    private World mockWorld;
    private Body mockBody;
    private Fixture mockFixture;
    private GL20 mockGl20;

    @BeforeEach
    void setup() throws NoSuchFieldException, IllegalAccessException {
        HeadlessApplicationConfiguration config = new HeadlessApplicationConfiguration();
        new HeadlessApplication(new ApplicationListener() {
            public void create() {}
            public void resize(int width, int height) {}
            public void render() {}
            public void pause() {}
            public void resume() {}
            public void dispose() {}
        }, config);

        // Mock GL20 to avoid actual OpenGL calls
        mockGl20 = mock(GL20.class);
        Field glField = Gdx.class.getDeclaredField("gl");
        glField.setAccessible(true);
        glField.set(null, mockGl20);

        // Setup other necessary mocks
        mockScreen = mock(PlayScreen.class);
        mockWorld = mock(World.class);
        mockBody = mock(Body.class);
        mockFixture = mock(Fixture.class);

        when(mockWorld.createBody(any())).thenReturn(mockBody);
        when(mockBody.createFixture(any())).thenReturn(mockFixture);
        when(mockBody.getPosition()).thenReturn(new Vector2(5, 5));

        when(mockScreen.getWorld()).thenReturn(mockWorld);
    }

   
    void testEnemy(AbstractEnemy enemy, AbstractEnemyView enemyView) {
        when(enemy.getBody().getPosition()).thenReturn(new Vector2(10, 10));
        enemyView.update(0.1f);
        assertEquals(10, enemyView.getX(), 0.1);
        assertEquals(10, enemyView.getY(), 0.1);

        // Testing movement
        when(enemy.getBody().getPosition()).thenReturn(new Vector2(20, 20));
        enemyView.update(0.1f);
        assertEquals(20, enemyView.getX(), 0.1);
        assertEquals(20, enemyView.getY(), 0.1);
    }

    @Test
    void testBlueEnemyView() {
        AbstractEnemy blueEnemy = new BlueEnemy(mockScreen, 5, 5, 20, 1.0f, 10);
        AbstractEnemyView blueEnemyView = new BlueEnemyView(blueEnemy);
        testEnemy(blueEnemy, blueEnemyView);
    }

    @Test
    void testRedEnemyView() {
        AbstractEnemy redEnemy = new RedEnemy(mockScreen, 5, 5, 25, 0.7f, 15);
        AbstractEnemyView redEnemyView = new RedEnemyView(redEnemy);
        testEnemy(redEnemy, redEnemyView);
    }
}
