package inf112.skeleton.app.view.screens;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.view.screens.PauseScreen;
import inf112.skeleton.app.view.screens.PlayScreen;

public class PauseScreenTest {
    private PauseScreen pauseScreen;
    private GameCreate mockGame;
    private SpriteBatch mockBatch;
    private GL20 mockGl20;

    @BeforeEach
    void setup() {
        // Set up headless application to avoid actual rendering
        HeadlessApplicationConfiguration config = new HeadlessApplicationConfiguration();
        new HeadlessApplication(new ApplicationListener() {
            public void create() {
            }

            public void resize(int width, int height) {
            }

            public void render() {
            }

            public void pause() {
            }

            public void resume() {
            }

            public void dispose() {
            }
        }, config);

        // Mock necessary OpenGL (GL20) objects
        mockGl20 = mock(GL20.class);
        Gdx.gl = mockGl20; // Set the mocked GL20 instance to Gdx.gl
        Gdx.gl20 = mockGl20;

        // Mock necessary objects
        mockGame = mock(GameCreate.class);
        mockBatch = mock(SpriteBatch.class);

        // Manually set the batch field if getBatch() might not be functioning correctly
        try {
            java.lang.reflect.Field batchField = GameCreate.class.getDeclaredField("batch");
            batchField.setAccessible(true);
            batchField.set(mockGame, mockBatch);
        } catch (Exception e) {
            e.printStackTrace();
        }

        when(mockGame.getBatch()).thenReturn(mockBatch); // Ensure this mock returns the expected batch

        // Create an instance of PauseScreen using mocked objects
        pauseScreen = new PauseScreen(mockGame, mock(PlayScreen.class));
    }

    @Test
    void pauseScreenNotNull() {
        assertNotNull(pauseScreen, "PauseScreen should not be null after initialization");
    }

    @Test
    void testShow() {
        pauseScreen.show();
    }

    @Test
    void testResize() {
        pauseScreen.resize(1, 1);
    }

    @Test
    void testHide() {
        pauseScreen.hide();
    }
}
