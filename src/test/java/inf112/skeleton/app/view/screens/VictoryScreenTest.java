package inf112.skeleton.app.view.screens;


import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.Viewport;

import inf112.skeleton.app.GameCreate;

import static org.mockito.Mockito.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.lang.reflect.Field;

class VictoryScreenTest {
    private VictoryScreen mockVictoryScreen;
    private GameCreate mockGame;
    private PlayScreen mockPlayScreen;
    private Stage mockStage;
    private Skin mockSkin;
    private OrthographicCamera mockCamera;
    private Viewport mockViewport;
    private Texture mockTexture;
    private Image mockImage;
    private SpriteBatch mockBatch;

    private GL20 mockGl20;

    @BeforeEach
    void setup() throws NoSuchFieldException, IllegalAccessException {
        HeadlessApplicationConfiguration config = new HeadlessApplicationConfiguration();
        new HeadlessApplication(new ApplicationListener() {
            public void create() {
            }

            public void resize(int width, int height) {
            }

            public void render() {
            }

            public void pause() {
            }

            public void resume() {
            }

            public void dispose() {
            }
        }, config);

        // Mock GL20 to avoid actual OpenGL calls
        mockGl20 = mock(GL20.class);
        Field glField = Gdx.class.getDeclaredField("gl");
        glField.setAccessible(true);
        glField.set(null, mockGl20);
        mockGame = mock(GameCreate.class);
        mockCamera = mock(OrthographicCamera.class);
        mockViewport = mock(Viewport.class);
        mockStage = mock(Stage.class);
        mockSkin = mock(Skin.class);
        mockTexture = mock(Texture.class);
        mockImage = mock(Image.class);
        mockBatch = mock(SpriteBatch.class);
        mockPlayScreen = mock(PlayScreen.class);

        when(mockGame.getBatch()).thenReturn(mockBatch);

        when(mockViewport.getCamera()).thenReturn(mockCamera);

        when(mockStage.getViewport()).thenReturn(mockViewport);
        when(mockStage.getBatch()).thenReturn(null);

        when(mockSkin.getAtlas()).thenReturn(null);

        when(mockTexture.getWidth()).thenReturn(0);
        when(mockTexture.getHeight()).thenReturn(0);

        when(mockImage.getX()).thenReturn(-450f);
        when(mockImage.getY()).thenReturn(0f);

        when(mockStage.getActors()).thenReturn(null);
        when(mockViewport.getCamera()).thenReturn(mockCamera);

        when(mockStage.getViewport()).thenReturn(mockViewport);
        when(mockStage.getBatch()).thenReturn(mockBatch);

        when(mockSkin.getAtlas()).thenReturn(null);

        when(mockTexture.getWidth()).thenReturn(0);
        when(mockTexture.getHeight()).thenReturn(0);

        when(mockImage.getX()).thenReturn(-450f);
        when(mockImage.getY()).thenReturn(0f);

        when(mockStage.getActors()).thenReturn(null);

        mockVictoryScreen = new VictoryScreen(mockGame, mockPlayScreen);

    }

    @Test
    void testImagePosition() {
        float expectedX = -450f;
        float expectedY = 0f;

        assertEquals(expectedX, mockImage.getX(), "Image X position is incorrect");
        assertEquals(expectedY, mockImage.getY(), "Image Y position is incorrect");
    }

    @Test
    void testResize() {
        mockVictoryScreen.resize(800, 416);
        mockVictoryScreen.getViewport().update(800, 416, true);
        assertEquals(mockVictoryScreen.getViewport().getScreenWidth(), 800);
        assertEquals(mockVictoryScreen.getViewport().getScreenHeight(), 416);
    }

    @Test
    void testPause() {
        mockVictoryScreen.pause();
    }

    @Test
    void testResume() {
        mockVictoryScreen.resume();
    }

    @Test
    void testHide() {
        mockVictoryScreen.hide();
    }

    @Test
    void testDispose() {
        mockVictoryScreen.dispose();
    }

    @Test
    void testGetViewport() {
        Viewport expectedViewport = mockViewport;
        assertEquals(expectedViewport, expectedViewport, "Viewport is incorrect");
    }

    @Test
    void testShow() {
        mockVictoryScreen.show();
    }


    @Test
    void testRender() {
        float fixedDeltaTime = 0.016f; // Fixed delta time for consistency
        
        Gdx.graphics = mock(Graphics.class);
        when(Gdx.graphics.getDeltaTime()).thenReturn(fixedDeltaTime);
        
        mockVictoryScreen = new VictoryScreen(mockGame, mockPlayScreen);
        when(mockGame.getBatch()).thenReturn(mockBatch);
        
        Gdx.gl = mock(GL20.class);
        mockVictoryScreen.stage = mockStage;
        mockVictoryScreen.render(fixedDeltaTime);

        verify(Gdx.gl).glClear(GL20.GL_COLOR_BUFFER_BIT);
        verify(mockStage).act(Math.min(fixedDeltaTime, 1 / 30f));
        verify(mockStage).draw();
    }
}

