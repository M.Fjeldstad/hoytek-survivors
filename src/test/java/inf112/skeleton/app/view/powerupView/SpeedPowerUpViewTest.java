package inf112.skeleton.app.view.powerupView;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.model.powerups.SpeedPowerUp;
import inf112.skeleton.app.view.powerupView.SpeedPowerUpView;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SpeedPowerUpViewTest {
    @Mock
    private SpeedPowerUp mockPowerUp;
    @Mock
    private Texture mockTexture;
    private SpeedPowerUpView SpeedPowerUpView;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        
        when(mockPowerUp.getPosition()).thenReturn(new Vector2(100, 100));
        when(mockTexture.getWidth()).thenReturn(64);
        when(mockTexture.getHeight()).thenReturn(64);
        
        SpeedPowerUpView = new SpeedPowerUpView(mockPowerUp, mockTexture);
    }

    @Test
    public void testInitialization() {
        assertNotNull(SpeedPowerUpView.getTexture(), "Texture should not be null");
        assertEquals(0.05f, SpeedPowerUpView.getScaleX(), 0.001, "Scale X should be set to 0.05f");
        assertEquals(0.05f, SpeedPowerUpView.getScaleY(), 0.001, "Scale Y should be set to 0.05f");

        float widthScaled = (mockTexture.getWidth() / GameCreate.PPM) ;
        float heightScaled = (mockTexture.getHeight() / GameCreate.PPM) ;

        float expectedX = (100 - widthScaled) / GameCreate.PPM;
        float expectedY = (100 - heightScaled) / GameCreate.PPM;
        assertEquals(expectedX, SpeedPowerUpView.getX(), 0.001, "X position should be correctly calculated");
        assertEquals(expectedY, SpeedPowerUpView.getY(), 0.001, "Y position should be correctly calculated");
    }
}
