package inf112.skeleton.app.view.powerupView;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import inf112.skeleton.app.GameCreate;
import inf112.skeleton.app.model.powerups.DamagePowerUp;
import inf112.skeleton.app.view.powerupView.DamagePowerUpView;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DamagePowerUpViewTest {
    @Mock
    private DamagePowerUp mockPowerUp;
    @Mock
    private Texture mockTexture;
    private DamagePowerUpView damagePowerUpView;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        
        when(mockPowerUp.getPosition()).thenReturn(new Vector2(100, 100));
        when(mockTexture.getWidth()).thenReturn(64);
        when(mockTexture.getHeight()).thenReturn(64);
        
        damagePowerUpView = new DamagePowerUpView(mockPowerUp, mockTexture);
    }

    @Test
    public void testInitialization() {
        assertNotNull(damagePowerUpView.getTexture(), "Texture should not be null");
        assertEquals(0.05f, damagePowerUpView.getScaleX(), 0.001, "Scale X should be set to 0.05f");
        assertEquals(0.05f, damagePowerUpView.getScaleY(), 0.001, "Scale Y should be set to 0.05f");

        float widthScaled = (mockTexture.getWidth() / GameCreate.PPM) * 4;
        float heightScaled = (mockTexture.getHeight() / GameCreate.PPM) * 4;

        float expectedX = (100 - widthScaled) / GameCreate.PPM;
        float expectedY = (100 - heightScaled) / GameCreate.PPM;
        assertEquals(expectedX, damagePowerUpView.getX(), 0.001, "X position should be correctly calculated");
        assertEquals(expectedY, damagePowerUpView.getY(), 0.001, "Y position should be correctly calculated");
    }
}
