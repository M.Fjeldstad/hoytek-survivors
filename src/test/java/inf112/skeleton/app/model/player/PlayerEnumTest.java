package inf112.skeleton.app.model.player;

import org.junit.jupiter.api.Test;

import inf112.skeleton.app.model.player.PlayerEnum;

import static org.junit.jupiter.api.Assertions.*;


public class PlayerEnumTest {

    @Test
    public void testPlayerEnumValues(){
        PlayerEnum[] values= PlayerEnum.values();
        assertEquals(PlayerEnum.STANDING, values[0]);
        assertEquals(PlayerEnum.LEFT, values[1]);
        assertEquals(PlayerEnum.RIGHT, values[2]);
        assertEquals(PlayerEnum.UP, values[3]);
        assertEquals(PlayerEnum.DOWN, values[4]);




    }
    
}
