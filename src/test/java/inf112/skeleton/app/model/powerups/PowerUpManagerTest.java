package inf112.skeleton.app.model.powerups;

import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.model.powerups.PowerUpManager;
import inf112.skeleton.app.model.powerups.SpeedPowerUp;
import inf112.skeleton.app.view.screens.PlayScreen;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test class for PowerUpManager.
 */
public class PowerUpManagerTest {
    private PlayScreen mockPlayScreen;
    private World world;
    private PlayerModel mockPlayerModel;
    private PowerUpManager powerUpManager;

    @BeforeEach
    public void setUp() {
        // Initialiserer en ny World med tyngdekraften satt til (0, 0)
        world = new World(new Vector2(0, 0), true);

        mockPlayScreen = mock(PlayScreen.class);
        when(mockPlayScreen.getWorld()).thenReturn(world);
        mockPlayerModel = mock(PlayerModel.class);

        powerUpManager = spy(new PowerUpManager(mockPlayScreen, mockPlayerModel));

        // Tillater den virkelige update-metoden å kjøres
        doCallRealMethod().when(powerUpManager).update(anyFloat());
    }



    @Test
    public void getPowerUpCollisionHandlerTest() {
        // Tester at getPowerUpCollisionHandler returnerer et ikke-null objekt
        assertFalse(powerUpManager.getPowerUpCollisionHandler() == null, "Collision handler skal ikke være null.");
    }

    @Test
    public void getPowerUpsTest() {
        // Tester at getPowerUps returnerer en ikke-null liste
        assertFalse(powerUpManager.getPowerUps() == null, "PowerUps-listen skal ikke være null.");
    }

    @Test
    public void updateDoesNotSpawnBeforeIntervalTest() {
        // Tester at update-metoden ikke spawner power-ups før intervallet er nådd
        float deltaTime = 1.0f; // mindre enn SPAWN_INTERVAL
        powerUpManager.update(deltaTime);
        assertTrue(powerUpManager.getPowerUps().isEmpty(), "PowerUps-listen skal være tom hvis intervallet ikke er nådd.");
    }

    @Test
    void updateTest() {
        // Tester at update-metoden spawner power-ups etter intervallet er nådd
        float deltaTime =  11; // større enn SPAWN_INTERVAL
        powerUpManager.update(deltaTime);
        assertFalse(powerUpManager.getPowerUps().isEmpty(), "PowerUps-listen skal ikke være tom hvis intervallet er nådd.");
    }

    @Test
    void getActivePowerUpsTest() {
        powerUpManager.getActivePowerUps().add(new SpeedPowerUp(mockPlayScreen, mockPlayerModel, 0, 0));
        assertTrue(powerUpManager.getActivePowerUps().size() == 1, "ActivePowerUps-listen skal ikke være null.");
    }

    @Test  
    void getPowerUpsToRemoveTest() {
        powerUpManager.getPowerUpsToRemove().add(new SpeedPowerUp(mockPlayScreen, mockPlayerModel, 0, 0));
        assertTrue(powerUpManager.getPowerUpsToRemove().size() == 1, "PowerUpsToRemove-listen skal ikke være null.");
    }
}
