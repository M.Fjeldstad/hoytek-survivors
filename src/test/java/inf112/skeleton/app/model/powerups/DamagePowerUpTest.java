package inf112.skeleton.app.model.powerups;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;

import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.model.powerups.DamagePowerUp;
import inf112.skeleton.app.view.screens.PlayScreen;

import static org.junit.jupiter.api.Assertions.*;

import org.mockito.MockitoAnnotations;

/**
 * Test class for DamagePowerUp.
 */
public class DamagePowerUpTest {
    @Mock
    private PlayScreen mockPlayScreen;
    @Mock
    private PlayerModel mockPlayerModel;
    @Mock
    private World mockWorld;
    @Mock
    private Body mockBody;
    @Mock
    private Fixture mockFixture;

    private DamagePowerUp damagePowerUp;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(mockPlayScreen.getWorld()).thenReturn(mockWorld);
        when(mockWorld.createBody(any())).thenReturn(mockBody);
        when(mockBody.createFixture(any())).thenReturn(mockFixture); // Ensure createFixture does not return null

        damagePowerUp = new DamagePowerUp(mockPlayScreen, mockPlayerModel, 100, 100);
    }

    @Test
    public void applyPowerUpEffectTest() {
        damagePowerUp.applyPowerUp();
        verify(mockPlayerModel, times(1)).addAttackDamage(5);
        assertTrue(damagePowerUp.isActive);
    }

    @Test
    public void removePowerUpEffectTest() {
        damagePowerUp.removePowerUp();
        verify(mockPlayerModel, times(1)).addAttackDamage(-5);
        assertFalse(damagePowerUp.isActive);
    }


    @Test
    public void powerUpDisposeTest() {
        damagePowerUp.dispose();
        // Verifying that dispose indeed calls necessary cleanup methods
        verify(mockWorld).destroyBody(mockBody);  // Confirm that the body is destroyed upon disposal
    }
}
