package inf112.skeleton.app.model.powerups;


import org.mockito.MockitoAnnotations;
import org.mockito.Mock;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.model.powerups.AbstractPowerUp;
import inf112.skeleton.app.model.powerups.PowerUpEnum;
import inf112.skeleton.app.view.screens.PlayScreen;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class AbstractPowerUpTest {

    @Mock
    private PlayScreen mockPlayScreen;
    @Mock
    private PlayerModel mockPlayerModel;
    @Mock
    private World mockWorld;
    @Mock
    private Body mockBody;
    @Mock
    private Fixture mockFixture;

    private AbstractPowerUp powerUp;

    @SuppressWarnings("deprecation")
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(mockPlayScreen.getWorld()).thenReturn(mockWorld);
        when(mockWorld.createBody(any(BodyDef.class))).thenReturn(mockBody);

        // Correct usage for non-void methods
        when(mockBody.createFixture(any(FixtureDef.class))).thenReturn(mockFixture);

        // If you had a void method setup issue, it should look something like this:
        // doNothing().when(mockSomeObject).someVoidMethod(any());

        // Create a concrete instance of the abstract class for testing
        powerUp = new AbstractPowerUp(mockPlayScreen, mockPlayerModel, PowerUpEnum.SPEED_BOOST, 100, 100) {
            @Override
            protected void removePowerUpEffect() {
                // Custom logic for test
            }
            @Override
            protected void applyPowerUpEffect() {
                // Custom logic for test
            }
        };
    }

    @Test
    public void applyPowerUpShouldSetActiveAndDispose() {
        powerUp.applyPowerUp();
        assertTrue(powerUp.isActive);
        verify(mockWorld).destroyBody(mockBody);
    }

    @Test
    public void disposeShouldDestroyBody() {
        powerUp.dispose();
        verify(mockWorld).destroyBody(mockBody);  // Verify that the body is destroyed
    }

    @Test
    public void getTypeShouldReturnCorrectType() {
        assertEquals(PowerUpEnum.SPEED_BOOST, powerUp.getType());
    }

    @Test
    public void getPositionShouldReturnInitialPosition() {
        assertEquals(new Vector2(100, 100), powerUp.getPosition());
    }
    
    
    @Test
    public void updateShouldNotDeactivatePowerUpWhenDurationNotExpired() {
        powerUp.isActive = true;
        powerUp.getPowerUpDuration();
        powerUp.setPowerUpDuration(5);  // Set a longer duration

        powerUp.update(1);  // Update with a time less than the duration

        assertTrue(powerUp.isActive, "PowerUp should still be active when duration has not expired yet");
        assertFalse(powerUp.isRemovable, "PowerUp should not be removable before duration expires");
        // Remove the verify line since powerUp is not a mock
    }

    @Test
    void getBodyTest() {
        assertEquals(mockBody, powerUp.getBody());
    }
}

