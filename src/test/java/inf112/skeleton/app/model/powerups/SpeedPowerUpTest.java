package inf112.skeleton.app.model.powerups;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;

import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.model.powerups.SpeedPowerUp;
import inf112.skeleton.app.view.screens.PlayScreen;

import static org.junit.jupiter.api.Assertions.*;

import org.mockito.MockitoAnnotations;

public class SpeedPowerUpTest {
    @Mock
    private PlayScreen mockPlayScreen;
    @Mock
    private PlayerModel mockPlayerModel;
    @Mock
    private World mockWorld;
    @Mock
    private Body mockBody;
    @Mock
    private Fixture mockFixture;

    private SpeedPowerUp speedPowerUp;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(mockPlayScreen.getWorld()).thenReturn(mockWorld);
        when(mockWorld.createBody(any())).thenReturn(mockBody);
        when(mockBody.createFixture(any())).thenReturn(mockFixture);

        speedPowerUp = new SpeedPowerUp(mockPlayScreen, mockPlayerModel, 100, 100);
    }

    @Test
    public void applyPowerUpEffectTest() {
        speedPowerUp.applyPowerUp();
        verify(mockPlayerModel, times(1)).addMaxSpeed(0.5f);
        verify(mockPlayerModel, times(1)).addSpeed(0.5f);
        assertTrue(speedPowerUp.isActive);
    }

    @Test
    public void removePowerUpEffectTest() {
        speedPowerUp.removePowerUp();
        verify(mockPlayerModel, times(1)).addMaxSpeed(-0.5f);
        verify(mockPlayerModel, times(1)).addSpeed(-0.5f);
        assertFalse(speedPowerUp.isActive);
    }

    @Test
    public void powerUpDisposeTest() {
        speedPowerUp.dispose();
        verify(mockWorld).destroyBody(mockBody);  // Confirm that the body is destroyed upon disposal
    }
}
