package inf112.skeleton.app.model.powerups;

import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.model.powerups.AbstractPowerUp;
import inf112.skeleton.app.model.powerups.DamagePowerUp;
import inf112.skeleton.app.model.powerups.PowerUpFactory;
import inf112.skeleton.app.model.powerups.SpeedPowerUp;
import inf112.skeleton.app.view.screens.PlayScreen;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Test class for PowerUpFactory.
 */
public class PowerUpFactoryTest {
    private PlayScreen mockPlayScreen;
    private World world;

    private PlayerModel mockPlayerModel;

    private PowerUpFactory powerUpFactory;

    @BeforeEach
    public void setUp() {
        world = new World(new Vector2(0, 0), true);

        mockPlayScreen = mock(PlayScreen.class);
        when(mockPlayScreen.getWorld()).thenReturn(world);
        mockPlayerModel = mock(PlayerModel.class);

        powerUpFactory = new PowerUpFactory(mockPlayScreen);
    }

    @Test
    public void createDamagePowerUpTest() {
        AbstractPowerUp powerUp = powerUpFactory.damagePowerUp(mockPlayerModel, 0, 0);

        assertTrue(powerUp instanceof DamagePowerUp);
    }

    @Test
    public void createSpeedPowerUpTest() {
        AbstractPowerUp powerUp = powerUpFactory.speedPowerUp(mockPlayerModel, 0, 0);

        assertTrue(powerUp instanceof SpeedPowerUp);
    }
}
