package inf112.skeleton.app.model.weapons.fireball;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.model.weapons.fireball.Fireball;
import inf112.skeleton.app.view.screens.PlayScreen;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class FireballTest {
    private PlayScreen mockPlayScreen;
    private PlayerModel mockPlayerModel;
    private World world;

    private Fireball fireball;

    @BeforeEach
    public void setUp() {
        // Manual creation of a World object without mocking.
        world = new World(new Vector2(0, 0), true);

        // Mock the PlayScreen to return the manually created World object.
        mockPlayScreen = mock(PlayScreen.class);
        when(mockPlayScreen.getWorld()).thenReturn(world);

        // Mock PlayerModel
        mockPlayerModel = new PlayerModel(mockPlayScreen);
        when(mockPlayScreen.getPlayerModel()).thenReturn(mockPlayerModel);

        // Initialize Fireball with the mocked PlayScreen.
        fireball = new Fireball(mockPlayScreen);
    }

    @Test
    public void testConstructor() {
        Vector2 expectedPosition = new Vector2(mockPlayerModel.getB2body().getPosition().x, mockPlayerModel.getB2body().getPosition().y);
        assertEquals(expectedPosition, fireball.getPosition(), "Fireball should be initialized at the player's position.");
    }

    @Test
    public void testSetLinearVelocity() {
        Vector2 velocity = new Vector2(10, 10);
        fireball.setLinearVelocity(velocity);
        assertEquals(velocity, fireball.getB2body().getLinearVelocity(), "Velocity should be set correctly.");
    }

    @Test
    public void testGetPosition() {
        assertEquals(fireball.getB2body().getPosition(), fireball.getPosition(), "Position should be retrieved correctly.");
    }

    @Test
    public void testDefineEntity() {
        assertNotNull(fireball.getB2body(), "Fireball should have a body after defining the entity.");
    }
}