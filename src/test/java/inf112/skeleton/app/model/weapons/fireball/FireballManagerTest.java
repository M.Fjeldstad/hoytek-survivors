package inf112.skeleton.app.model.weapons.fireball;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.lang.reflect.Field;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.model.weapons.fireball.Fireball;
import inf112.skeleton.app.model.weapons.fireball.FireballManager;
import inf112.skeleton.app.tools.listeners.FireballCollisionHandler;
import inf112.skeleton.app.view.screens.PlayScreen;

public class FireballManagerTest {

    private PlayScreen mockPlayScreen;
    private FireballManager manager;
    private PlayerModel mockPlayerModel;
    private World world;
    private OrthographicCamera mockCamera;
    private Vector3 mockCursorPosition;

    @BeforeEach
    public void setUp()
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        HeadlessApplicationConfiguration config = new HeadlessApplicationConfiguration();
        new HeadlessApplication(new ApplicationListener() {
            public void create() {
            }

            public void resize(int width, int height) {
            }

            public void render() {
            }

            public void pause() {
            }

            public void resume() {
            }

            public void dispose() {
            }
        }, config);

        // Mock GL20 to avoid actual OpenGL calls
        GL20 mockGl20 = mock(GL20.class);
        Field glField = Gdx.class.getDeclaredField("gl");
        glField.setAccessible(true);
        glField.set(null, mockGl20);

        // If you use Gdx.files, mock it as well
        Gdx.files = mock(com.badlogic.gdx.Files.class);

        world = new World(new Vector2(0, 0), true);
        mockCursorPosition = mock(Vector3.class);

        mockCamera = mock(OrthographicCamera.class);
        // Mock the PlayScreen to return the manually created World object.
        mockPlayScreen = mock(PlayScreen.class);
        when(mockPlayScreen.getWorld()).thenReturn(world);
        when(mockPlayScreen.getGameCam()).thenReturn(mockCamera);
        when(mockPlayScreen.getCursorPosition()).thenReturn(mockCursorPosition);

        // Mock PlayerModel
        mockPlayerModel = new PlayerModel(mockPlayScreen);
        when(mockPlayScreen.getPlayerModel()).thenReturn(mockPlayerModel);

        // Mock FireballManager
        manager = new FireballManager(mockPlayScreen);
    }

    @Test
    public void testConstructor() {
        assertNotNull(manager, "Manager should be instantiated");
    }

    @Test
    public void testGetFireballCollisionHandler() {
        assertNotNull(manager.getFireballCollisionHandler(),
                "getFireballCollisionHandler should return a FireballCollisionHandler");
        assertTrue(manager.getFireballCollisionHandler() instanceof FireballCollisionHandler);
    }

    @Test
    public void testGetFireball() {
        assertNotNull(manager.getFireballs(), "getFireball Should return an array of Fireballs");
        assertTrue(manager.getFireballs() instanceof Array<Fireball>);
    }

    @Test
    public void testGetSpawnInterval() {
        assertNotNull(manager.getSpawnInterval(), "getFireballViews should return a float value");
        assertTrue(manager.getSpawnInterval() instanceof Float);
    }

    @Test
    public void testSetSpawnInterval() {
        float interval = 2.0f;
        manager.setSpawnInterval(interval);
        assertEquals(manager.getSpawnInterval(), interval, "Spawn interval should be set correctly.");
    }

    @Test
    void updateTest() {
        float deltaTime = 11.0f; // greater than SPAWN_INTERVAL
        manager.update(deltaTime);
        assertFalse(manager.getFireballs().isEmpty(), "Fireballs list should not be empty if interval is reached.");
    }

    @Test
    void getFireballsToRemoveTest() {
        manager.getFireballsToRemove().add(new Fireball(mockPlayScreen));
        assertTrue(manager.getFireballsToRemove().size == 1, "FireballsToRemove list should not be empty.");
    }

}