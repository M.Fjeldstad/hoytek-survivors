package inf112.skeleton.app.model.weapons.fireball;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.model.weapons.fireball.FireballFactory;
import inf112.skeleton.app.view.screens.PlayScreen;

public class FireballFactoryTest {

    private PlayScreen mockPlayScreen;
    private FireballFactory factory;
    private PlayerModel mockPlayerModel;
    private World world;

    @BeforeEach
    public void setUp() {
        world = new World(new Vector2(0, 0), true);

        // Mock the PlayScreen to return the manually created World object.
        mockPlayScreen = mock(PlayScreen.class);
        when(mockPlayScreen.getWorld()).thenReturn(world);

        // Mock PlayerModel
        mockPlayerModel = new PlayerModel(mockPlayScreen);
        when(mockPlayScreen.getPlayerModel()).thenReturn(mockPlayerModel);

        // Mock FireballFactory
        factory = new FireballFactory(mockPlayScreen);
    }

    @Test
    public void testConstructor() {
        assertNotNull(factory, "Factory should be instantiated");
    }

    @Test
    public void testCreateFireball() {
        assertNotNull(factory.createFireball(), "createFireball should return a Fireball");
    }
}
