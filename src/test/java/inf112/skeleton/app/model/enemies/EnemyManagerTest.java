package inf112.skeleton.app.model.enemies;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import inf112.skeleton.app.model.enemies.EnemyManager;
import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.view.screens.PlayScreen;

import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Field;

class EnemyManagerTest {

    private PlayScreen mockPlayScreen;
    private World world;

    private PlayerModel mockPlayerModel;

    private EnemyManager enemyManager;

    @BeforeEach
    void setup() throws NoSuchFieldException, IllegalAccessException {
        HeadlessApplicationConfiguration config = new HeadlessApplicationConfiguration();
        new HeadlessApplication(new ApplicationListener() {
            public void create() {}
            public void resize(int width, int height) {}
            public void render() {}
            public void pause() {}
            public void resume() {}
            public void dispose() {}
        }, config);

        // Mock GL20 to avoid actual OpenGL calls
        GL20 mockGl20 = mock(GL20.class);
        Field glField = Gdx.class.getDeclaredField("gl");
        glField.setAccessible(true);
        glField.set(null, mockGl20);

        // If you use Gdx.files, mock it as well
        Gdx.files = mock(com.badlogic.gdx.Files.class);

        world = new World(new Vector2(0, 0), true);

        mockPlayScreen = mock(PlayScreen.class);
        when(mockPlayScreen.getWorld()).thenReturn(world);
        mockPlayerModel = mock(PlayerModel.class);

        enemyManager = new EnemyManager(mockPlayScreen);
    }

    @Test
    void getEnemyCollisionHandler() {
        assertNotNull(enemyManager.getEnemyCollisionHandler());
    }

    @Test
    void getEnemiesToRemove() {
        assertNotNull(enemyManager.getEnemiesToRemove());
    }

    @Test
    void getEnemies() {
        assertNotNull(enemyManager.getEnemies());
    }


    @Test
    void getWaves() {
        assertEquals(enemyManager.getWaves(), 0);
    }

    @Test
    void getCurrentWave() {
        assertEquals(enemyManager.getWaves(), 0);
    }

    @Test
    void updateTest() {
        enemyManager.update(1);
        assertEquals(enemyManager.getWaves(), 1);
    }

    @Test
    void spawnEnemiesTest() {
        enemyManager.spawnEnemies();
        assertEquals(enemyManager.getEnemies().size, 5);
    }
}
