package inf112.skeleton.app.model.enemies;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

import inf112.skeleton.app.model.enemies.AbstractEnemy;
import inf112.skeleton.app.model.enemies.BlueEnemy;
import inf112.skeleton.app.model.enemies.RedEnemy;
import inf112.skeleton.app.model.player.PlayerModel;
import inf112.skeleton.app.view.screens.PlayScreen;

import com.badlogic.gdx.math.Vector2;

public class EnemyTests {
    private PlayScreen mockScreen;
    private World mockWorld;
    private Body mockBody, mockPlayerBody;
    private Fixture mockFixture;
    private PlayerModel mockPlayerModel;

    @BeforeEach
    public void setup() {
        mockScreen = mock(PlayScreen.class);
        mockWorld = mock(World.class);
        mockBody = mock(Body.class);
        mockPlayerBody = mock(Body.class);
        mockFixture = mock(Fixture.class);

        // Setup mock to return a mock Body when a BodyDef is passed
        when(mockWorld.createBody(any())).thenReturn(mockBody);

        // Optionally, return a mock Fixture when createFixture is called
        when(mockBody.createFixture(any())).thenReturn(mockFixture);

        // Ensure the mock returns itself when methods like getPosition are called
        when(mockBody.getPosition()).thenReturn(new Vector2(100, 100));

        when(mockScreen.getWorld()).thenReturn(mockWorld);
    }

    @Test
    public void testEnemyInitialization() {
        AbstractEnemy enemy = new RedEnemy(mockScreen, 100, 100, 20, 1.0f, 5);
        assertEquals(20, enemy.getHealth());
        assertEquals(1.0f, enemy.getMovementSpeed(), 0.01);
        assertEquals(5, enemy.getAttackDamage());
    }

    @Test
    public void testHealthModification() {
        AbstractEnemy enemy = new RedEnemy(mockScreen, 100, 100, 20, 1.0f, 5);
        enemy.addHealth(-5);
        assertEquals(15, enemy.getHealth());
        enemy.addHealth(10);
        assertEquals(25, enemy.getHealth());
    }

    @Test
    public void testEnemySpawnProperties() {
        AbstractEnemy enemy = new BlueEnemy(mockScreen, 150, 150, 25, 0.7f, 10);
        assertEquals(25, enemy.getHealth());
        assertEquals(0.7f, enemy.getMovementSpeed(), 0.01);
        assertEquals(10, enemy.getAttackDamage());
    }

    @Test
    public void testEnemyHealthReductionOnCollision() {
        AbstractEnemy enemy = new BlueEnemy(mockScreen, 150, 150, 25, 0.7f, 10);
        enemy.addHealth(-5); // Enemy takes 5 damage
        assertEquals(20, enemy.getHealth());
    }

    @Test
    public void testEnemyDisposal() {
        AbstractEnemy enemy = new BlueEnemy(mockScreen, 150, 150, 25, 0.7f, 10);
        enemy.dispose();
        verify(mockWorld).destroyBody(enemy.getBody());
    }
}
